﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace StringLib
{
    public class StringLine
    {
        private string stringSentence = "";

        /*public Line(string stringSentence = "")
        {
             this.stringSentence = stringSentence;
        }*/

        /// <summary>
        /// Метод, который вводит строку.
        /// </summary>
        /// <param name="stringSentence">принимаемая строка</param>
        public void LineInput(string stringSentence)
        {
            this.stringSentence = stringSentence;
        }

        /// <summary>
        /// Метод, который выводит строку.
        /// </summary>
        /// <returns>Результат - возращаемая строка</returns>
        public override string ToString()
        {
            return stringSentence;
        }

        // <summary>
        /// Метод, что удаляет из строки все лишние пробелы, оставив между словами не более одного и помещает результат в новую строку.
        /// </summary>
        /// <returns>Результат - новая преобразованная строка.</returns>
        public string RemoveExtraSpaces()
        {
            string s2, newConventedString;
            newConventedString = stringSentence;
            do
            {
                s2 = newConventedString;
                newConventedString = s2.Replace("  ", " ");
            }
            while (s2 != newConventedString);

            return newConventedString;
        }

        /// <summary>
        /// Метод, который формирует одномерный StringBuilder массив строк в котором не содержится дат, а также отсортированы в строках слова по алфавиту. 
        /// </summary>
        /// <returns>Результат - новый сформированный StringBuilder массив строк в котором не содержится дат и отсортированы в строках слова по алфавиту.</returns>
        public StringBuilder[] FormationOfAOneDimensionalStringBuilderArray()
        {
            string[] stringArray = Regex.Split(stringSentence, @"[\!\.\?][\s]");

            for(int i = 0; i < stringArray.Length; i++)
            {
                string unsortedSentences = stringArray[i];
                string[] sortedArrayOfSentences = unsortedSentences.Split(' ');
                Array.Sort(sortedArrayOfSentences);
                stringArray[i] = "";
                for (int j = 0; j < sortedArrayOfSentences.Length; j++)
                {
                    stringArray[i] += " " + sortedArrayOfSentences[j];

                }
            }

            StringBuilder[] stringBuilderArray = new StringBuilder[stringArray.Length];

            for (int i = 0; i < stringBuilderArray.Length; i++)
            {
                stringBuilderArray[i] = new StringBuilder(stringArray[i]);
            }

            for (int i = 0; i < stringBuilderArray.Length; i++)
            {
                if (Regex.Match(stringBuilderArray[i].ToString(), @"(0[1-9]|[12][0-9]|3[01])[\\\/\.](0[1-9]|1[012])[\\\/\.](((1[0-9]|20)\d\d)|([0-9][0-9]))").Success)
                {
                    stringBuilderArray[i] = null;
                }
            }

            return stringBuilderArray;
        }
    }
}


