﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Globalization;

namespace SalaryLib
{

    public class Staff
    {
        public string name { get; set; }
        public string direction { get; set; }
        public string salaryValue { get; set; }

        public Staff(string stringSentence)
        {

            string FullName = @"^[А-Я][а-я]+\s[А-Я][а-я]+\s[А-Я][а-я]+";
            string Hours = @"\d+\s[часов]";
            string Salary = @"\d+[\.,]\d+";

            for (int i = 0; i < stringSentence.Length; i++)
            {
                name = Regex.Match(stringSentence, FullName).ToString();
                direction = Regex.Match(stringSentence, Hours).ToString();
                salaryValue = Regex.Match(stringSentence, Salary).ToString();
            }
        }
    }

    public class Parser
    {
        public string[] arrayWithoutSalaries = new string[1000];
        public int countStaff = 0;
        Staff[] arrayOfStaff = new Staff[1000];

        /// <summary>
        /// Создание массива обьектов.
        /// </summary>
        /// <param name="stringSentence">Строка, которая содержит данные сотрудиков</param>
        /// <returns>Кол-во элементов массива.</returns>
        public string AddingEmployee(string stringSentence )
        {
            string[] array = stringSentence.Split(';');
            for (int i = 0; i < array.Length; i++)
            {
                arrayOfStaff[i] = new Staff(array[i]);
                countStaff++;
            }
            return countStaff.ToString();
        }

        /// <summary>
        /// Метод, который проверяет на наличие значений зарплат.
        /// </summary>
        /// <returns>Массив, который содержит вещественные значения зарплат сотрудников</returns>
        public string[] Salary()
        {
            for (int i = 0; i < countStaff; i++)
            {
                arrayWithoutSalaries[i] = arrayOfStaff[i].salaryValue;
            }
            return arrayWithoutSalaries;
        }

        /// <summary>
        /// Метод, который вычисляет общую сумму зарплат сотрудников. 
        /// </summary>
        /// <param name="arrayOfSalaryValues">Одномерный массив, в котором содержатся вещественные значения зарплат.</param>
        /// <returns>Результат - возвращает значение общей суммы зарплат сотрудников.</returns>
        public double CalculatingTotalSalary(double[] arrayOfSalaryValues)
        {
            double totalSalary = 0;
            int amountOfSalaries = arrayOfSalaryValues.Length;
            for (int i = 0; i < arrayOfSalaryValues.Length; i++)
            {
                totalSalary += arrayOfSalaryValues[i];
            }
            return totalSalary;
        }

        /// <summary>
        /// Метод, который вычисляет среднюю сумму зарплат сотрудников. 
        /// </summary>
        /// <param name="arrayOfSalaryValues">Одномерный массив, в котором содержатся вещественные значения зарплат.</param>
        /// <returns>Результат - возвращает значение средней суммы зарплат сотрудников.</returns>
        public double CalculatingAverageSalary(double[] arrayOfSalaryValues)
        {
            return CalculatingTotalSalary(arrayOfSalaryValues) / countStaff;
        }

        /// <summary>
        /// Метод, который формирует одномерный массив вещественных чисел, в котором содержатся значения зарплат сотрудников. 
        /// </summary>
        /// <returns>Результат - сформированный одномерный массив вещественных чисел в котором содержатся значения зарплат сотрудников.</returns>
        public double[] FormationOfAnArrayOfSalaryValues()
        {
            double[] arrayOfSalaryValues = new double[arrayWithoutSalaries.Length];

            for (int i = 0; i < arrayWithoutSalaries.Length; i++)
            {
                arrayOfSalaryValues[i] = Convert.ToDouble(arrayWithoutSalaries[i]);
            }
            return arrayOfSalaryValues;
        }
    }
}



/*using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace SalaryLib
{
    public class StrSalary
    {
        private string stringSentence = "";

        //komment
        public Line(string stringSentence = "")
        {
             this.stringSentence = stringSentence;
        }

        /// <summary>
        /// Метод, который вводит строку
        /// </summary>
        /// <param name="stringSentence">принимаемая строка</param>
        public void LineInput(string stringSentence)
        {
            this.stringSentence = stringSentence;
        }

        /// <summary>
        /// Метод, который выводит строку.
        /// </summary>
        /// <returns>Результат - возращаемая строка</returns>
        public override string ToString()
        {
            return stringSentence;
        }

        /// <summary>
        /// Метод, который ищет и извлекает из строки значения зарплат и формирует одномерный массив вещественных чисел в котором содержатся значения зарплат сотрудников. 
        /// </summary>
        /// <returns>Результат - сформированный одномерный массив вещественных чисел в котором содержатся значения зарплат сотрудников.</returns>
        public double[] FormationOfAnArrayOfSalaryValues()
        {
            StringBuilder str = new StringBuilder(stringSentence);
            Regex patternSalary = new Regex(@"\d+[\.,]\d+");
            MatchCollection salaries = patternSalary.Matches(str.ToString());

            double[] arrayOfSalaryValues = new double[salaries.Count];

            for (int i = 0; i < arrayOfSalaryValues.Length; i++)
            {
                arrayOfSalaryValues[i] = double.Parse(salaries[i].Value);
            }

            return arrayOfSalaryValues;
        }

        /// <summary>
        /// Метод, который вычисляет общую сумму зарплат сотрудников. 
        /// </summary>
        /// <param name="arrayOfSalaryValues">Одномерный массив, в котором содержатся вещественные значения зарплат.</param>
        /// <returns>Результат - возвращает значение общей суммы зарплат сотрудников.</returns>
        public double CalculatingTotalSalary(double[] arrayOfSalaryValues)
        {
            double totalSalary = 0;
            int amountOfSalaries = arrayOfSalaryValues.Length;
            for (int i = 0; i < arrayOfSalaryValues.Length; i++)
            {
                totalSalary += arrayOfSalaryValues[i];
            }

            return totalSalary;
        }

        /// <summary>
        /// Метод, который вычисляет среднюю сумму зарплат сотрудников. 
        /// </summary>
        /// <param name="arrayOfSalaryValues">Одномерный массив, в котором содержатся вещественные значения зарплат.</param>
        /// <returns>Результат - возвращает значение средней суммы зарплат сотрудников.</returns>
        public double CalculatingAverageSalary(double[] arrayOfSalaryValues)
        {
            return CalculatingTotalSalary(arrayOfSalaryValues) / arrayOfSalaryValues.Length;
        }

        //nado za komment
        public double CalculatingTotalAndAverageSalary()
        {
            Regex patternSalary = new Regex(@"\d+[\.,]\d+");
            MatchCollection salaries = patternSalary.Matches(stringSentence);

            double[] arrayOfSalaryValues = new double[salaries.Count];
            double totalSalary = 0;
            for (int i = 0; i < arrayOfSalaryValues.Length; i++)
            {
                arrayOfSalaryValues[i] = double.Parse(salaries[i].Value);
                totalSalary += arrayOfSalaryValues[i];
                //Console.WriteLine(arrayOfSalaryValues[i]);
            }
            double averageSalary = totalSalary / arrayOfSalaryValues.Length; 
            return totalSalary;
        }

    }
}*/
