﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SalaryLib;

namespace OOPCSharp5
{
    class Program
    {
        static void Main(string[] args)
        {

            int choiceAction;
            int countStaff = 0;
            Parser parser = new Parser();
            double[] arrayOfSalaryValues;
            
            string stringSentence = "Иванов Иван Иванович, 2,5 руб, 56 часов;Свяцкий Илья Петрович, 10,5 руб, 51 часов;Соболев Денис Викторович, 4,5 руб, 79 часов";

            do
            {
                //меню
                Console.Clear();
                Console.WriteLine("___________________________________________________________________________");
                Console.WriteLine("|                                                                         |");
                Console.WriteLine("|                                   МЕНЮ                                  |");
                Console.WriteLine("|_________________________________________________________________________|");
                Console.WriteLine("|                                                                         |");
                Console.WriteLine("| 1. Создать строку.                                                      |");
                Console.WriteLine("| 2. Вывести найденные зарплаты и вычислить общую и среднюю сумму зарплат.|");
                Console.WriteLine("| 3. Выход из программы.                                                  |");
                Console.WriteLine("|_________________________________________________________________________|");

                Console.WriteLine();
                Console.WriteLine("Введите пункт меню (1-2): ");

                choiceAction = Convert.ToInt32(Console.ReadLine()); //ввод выбора

                switch (choiceAction)
                {
                    case 1:
                        {
                           countStaff = Convert.ToInt32(parser.AddingEmployee(stringSentence));

                            break;
                        }

                    case 2:
                        {
                            string[] arrayWithoutSalaries = new string[1000];
                            arrayWithoutSalaries = parser.Salary();

                            Console.WriteLine("Все найденные зарплаты сотрудников: ");
                            parser.FormationOfAnArrayOfSalaryValues();
                            arrayOfSalaryValues = parser.FormationOfAnArrayOfSalaryValues();

                            for (int i = 0; i < countStaff; i++)
                            {
                                Console.Write("{0}. {1}", i + 1, arrayOfSalaryValues[i] + " руб. " + " ");
                            }

                            Console.WriteLine();
                            Console.WriteLine("Общая сумма всех зарплат сотрудников: " + parser.CalculatingTotalSalary(arrayOfSalaryValues) + " руб.");
                            Console.WriteLine();
                            Console.WriteLine("Средняя сумма всех зарплат сотрудников: " + parser.CalculatingAverageSalary(arrayOfSalaryValues) + " руб.");
                            Console.ReadLine();

                            break;
                        }

                    default:
                        Console.WriteLine("Неверный пункт меню!");

                        break;
                }
                Console.ReadKey();
            }
            while (choiceAction != 3);
        }
    }
}



/*using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SalaryLib;

namespace OOPCSharp5
{
    class Program
    {
        static void Main(string[] args)
        {
            int choiceAction;
            string stringSentence = "";
            double[] arrayOfSalaryValues;

            StrSalary ParseSalary = new StrSalary();

            do
            {
                //меню
                Console.Clear();
                Console.WriteLine("___________________________________________________________________________");
                Console.WriteLine("|                                                                         |");
                Console.WriteLine("|                                   МЕНЮ                                  |");
                Console.WriteLine("|_________________________________________________________________________|");
                Console.WriteLine("|                                                                         |");
                Console.WriteLine("| 1. Вывести найденные зарплаты и вычислить общую и среднюю сумму зарплат.|");
                Console.WriteLine("| 2. Выход из программы.                                                  |");
                Console.WriteLine("|_________________________________________________________________________|");

                Console.WriteLine();
                Console.WriteLine("Введите пункт меню (1-2): ");

                choiceAction = Convert.ToInt32(Console.ReadLine()); //ввод выбора

                switch (choiceAction)
                {
                    case 1:
                        {
                            //Console.WriteLine("Введите строку: ");
                            //stringSentence = Console.ReadLine();
                            stringSentence = " Ф.И.О - Иванов И.Г, Кол-во отработанных часов - 168, Зарплата - 1,2 руб.; " +
                                             " Ф.И.О - Петров В.П; Кол-во отработанных часов - 160, Зарплата - 9,1 руб.; " +
                                             " Ф.И.О - Сидоров О.В, Кол-во отработанных часов - 165, Зарплата - 9,2 руб.; " +
                                             " Ф.И.О - Тихонов Д.С; Кол-во отработанных часов - 157, Зарплата - 2,5 руб.; ";
                                             " Ф.И.О - Никитин П.А; Кол-во отработанных часов - 157, Зарплата - 8,988 руб.; " +
                                             " Ф.И.О - Витальев И.Н; Кол-во отработанных часов - 159, Зарплата - 9,101 руб.; " +
                                             " Ф.И.О - Петров Г.Х; Кол-во отработанных часов - 154, Зарплата - 8,546 руб.; " +
                                             " Ф.И.О - Кравченко П.Л; Кол-во отработанных часов - 170, Зарплата - 9,519 руб.; " +
                                             " Ф.И.О - Буров А.Н; Кол-во отработанных часов - 155, Зарплата - 8,690 руб.; " +
                                             " Ф.И.О - Анатольев С.А; Кол-во отработанных часов - 160, Зарплата - 9,199 руб.; " +
                                             " Ф.И.О - Бедняков Н.Н; Кол-во отработанных часов - 156, Зарплата - 8,737 руб.; ";

                            ParseSalary.LineInput(stringSentence);
                            Console.WriteLine(ParseSalary.ToString());
                            Console.WriteLine();
                            Console.WriteLine("Все найденные зарплаты сотрудников: ");
                            ParseSalary.FormationOfAnArrayOfSalaryValues();
                            arrayOfSalaryValues = ParseSalary.FormationOfAnArrayOfSalaryValues();
                            ArrayOfSalaryValuesOutput(arrayOfSalaryValues);
                            Console.WriteLine();
                            Console.WriteLine("Общая сумма всех зарплат сотрудников: " + ParseSalary.CalculatingTotalSalary(arrayOfSalaryValues) + " руб.");
                            Console.WriteLine();
                            Console.WriteLine("Средняя сумма всех зарплат сотрудников: " + ParseSalary.CalculatingAverageSalary(arrayOfSalaryValues) + " руб.");
                            Console.ReadLine();

                            break;
                        }   
                    default:
                        Console.WriteLine("Неверный пункт меню!");

                        break;
                }
                Console.ReadKey();
            }
            while (choiceAction != 2);
        }
        
        /// <summary>
        /// Метод, который выводит зарплаты сотрудников.
        /// </summary>
        /// <param name="arrayOfSalaryValues">Одномерный массив, в котором содержатся вещественные значения зарплат.</param>
        public static void ArrayOfSalaryValuesOutput(double[] arrayOfSalaryValues)
        {
            for (int i = 0; i < arrayOfSalaryValues.Length; i++)
            {
                Console.Write("{0}. {1}", i + 1, arrayOfSalaryValues[i] + " ");
            }
        }
    }
}*/
