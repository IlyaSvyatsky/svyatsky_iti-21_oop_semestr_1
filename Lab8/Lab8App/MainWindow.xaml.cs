﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Lab8Library;

namespace Lab8App
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public FigureCollection figureCollection;
        public void MainForm()
        {
            InitializeComponent();
            figureCollection = new FigureCollection();
        }
        public void UpdateListBox()
        {

            FigureBox.Items.Clear();
            FigureBox.ItemsSource = null;
            int i = 0;
            List<string> figureList = new List<string>();
            for (i = 0; i < figureCollection.arrayOfFigures.Length; i++)
            {
                System.Windows.Media.Color mediaColor = System.Windows.Media.Color.FromArgb(figureCollection.arrayOfFigures[i].ColorOfFigure.A, figureCollection.arrayOfFigures[i].ColorOfFigure.R, figureCollection.arrayOfFigures[i].ColorOfFigure.G, figureCollection.arrayOfFigures[i].ColorOfFigure.B);
                if (figureCollection.GetQuaterInt(i)== 2 && figureCollection.IsRectangle(i) == true)
                {
                    mediaColor = System.Windows.Media.Color.FromArgb(255,0,255,0);
                }
                
                figureList.Add(figureCollection.arrayOfFigures[i].Display());
                //Background = new SolidColorBrush(mediaColor)
                FigureBox.Items.Add(new ListBoxItem { Content = figureCollection.arrayOfFigures[i].Display(), Background = new SolidColorBrush(mediaColor) });
            }
            FigureBox.Items.Refresh();
            //FigureBox.ItemsSource = figureList;
        }

        private void ReadFromFile_Click(object sender, RoutedEventArgs e)
        {
            figureCollection = new FigureCollection();
            figureCollection.AddFigures(); 
            Array.Sort(figureCollection.arrayOfFigures, new FigureComparer());
            UpdateListBox();
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                figureCollection.DeleteFigure(FigureBox.SelectedIndex);
                FigureBox.Items.RemoveAt(FigureBox.SelectedIndex);
            }
            catch
            { MessageBox.Show("Выберите фигуру для удаления"); }
            UpdateListBox();
        }

        private void GetQuater_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int index = FigureBox.SelectedIndex;
                MessageBox.Show(figureCollection.GetQuater(index));
            }
            catch
            { MessageBox.Show("Выберите фигуру для вычесления координатной четверти"); }
        }
    }
}
