﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StringLib;
using System.Text.RegularExpressions;

namespace OOPCSharp4
{
    class Program
    {
        static void Main(string[] args)
        {
            int choiceAction;
            string stringSentence = "";

            StringLine stringWithExtraSpaces = new StringLine();
            StringLine stringBuilderSentence = new StringLine();

            do
            {
                //меню
                Console.Clear();
                Console.WriteLine("___________________________________________________________________________");
                Console.WriteLine("|                                                                         |");
                Console.WriteLine("|                                   МЕНЮ                                  |");
                Console.WriteLine("|_________________________________________________________________________|");
                Console.WriteLine("|                                                                         |");
                Console.WriteLine("| 1. Удалить из строки все лишние пробелы.                                |");
                Console.WriteLine("|                                                                         |");
                Console.WriteLine("| 2. Сформировать StringBuilder массив строк в котором не содержится дат. |");
                Console.WriteLine("|                                                                         |");
                Console.WriteLine("| 3. Выход из программы.                                                  |");
                Console.WriteLine("|_________________________________________________________________________|");

                Console.WriteLine();
                Console.WriteLine("Введите пункт меню (1-3): ");

                choiceAction = Convert.ToInt32(Console.ReadLine()); //ввод выбора

                switch (choiceAction)
                {
                    case 1:
                        {
                             Console.WriteLine("Введите строку с лишними пробелами: ");
                             stringSentence = Console.ReadLine();
                             stringWithExtraSpaces.LineInput(stringSentence);
                             Console.WriteLine();
                             Console.WriteLine("Новая строка без лишних пробелов: " + stringWithExtraSpaces.RemoveExtraSpaces());
                             Console.ReadLine();

                            break;
                        }
                    case 2:
                        {
                            stringBuilderSentence = new StringLine();
                            //Console.WriteLine("Введите текст с датами: ");
                            StringBuilder[] stringBuilderArray;
                            // stringSentence = Console.ReadLine();

                            stringSentence = " Итак, это пробный текст по истории ВОВ! " +
                            " 10.05.1940 года Германия силами 135 дивизий вторгается в Бельгию, Нидерланды и Люксембург. " +
                            " Первая группа союзных армий 15/05/1940 выдвигается на территорию Бельгии, но  16.05.1940 не успевает помочь голландцам? " +
                            " Поскольку немецкая группа армий «Б» осуществляет стремительный бросок в Южную Голландию и уже 18.05.40 года захватывает Роттердам. " +
                            " Уже в мае Роттердам подвергается массированным бомбардировкам, что приводит к огромным разрушениям и жертвам среди мирного населения! " +
                            " После угрозы аналогичных бомбардировок Амстердама и Гааги правительство Нидерландов капитулирует 20/05/1940. " +
                            " В Бельгии немецкие десантники 10.06.1940, 11/06/1940 и 12.06.40 захватывают мосты через канал Альберта? " +
                            " Бельгия находилась в очень критической ситуации, шансов сопротивления не было? " +
                            " 15/06/1940 года это дает возможность крупным немецким танковым силам форсировать его до подхода союзников и выйти на Бельгийскую равнину 16.06.1940. " +
                            " Брюссель пал 17.06.40! " +
                            " Конец, спасибо за внимание. ";

                            stringBuilderSentence.LineInput(stringSentence);
                            Console.WriteLine();
                            stringBuilderArray = stringBuilderSentence.FormationOfAOneDimensionalStringBuilderArray();
                            Console.WriteLine("Сформирован StringBuilder массив строк в котором не содержится дат: ");
                            StringBuilderArrayOutput(stringBuilderArray);
                            Console.ReadLine();

                            break;
                        }
                    default:
                            Console.WriteLine("Неверный пункт меню!");

                        break;
                }
                Console.ReadKey();
            }
            while (choiceAction != 4);
        }

        /// <summary>
        /// Метод, который выводит новый сформированный StringBuilder массив строк в котором не содержится дат.
        /// </summary>
        /// <param name="stringBuilderArray">StringBuilder массив строк в котором не содержится дат</param>
        public static void StringBuilderArrayOutput(StringBuilder[] stringBuilderArray)
        {
            for (int i = 0; i < stringBuilderArray.Length; i++)
            {
                if (stringBuilderArray[i] != null)
                {
                    Console.WriteLine(stringBuilderArray[i]);
                    //Console.Write("{0}. {1}", i + 1, , stringBuilderArray[i]);
                }
            }
        }

    }
}



