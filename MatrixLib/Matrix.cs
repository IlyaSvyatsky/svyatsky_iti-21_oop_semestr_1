﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatrixLib
{
    public class Matrix
    {
        //массив элементов матрицы
        private int[,] array;

        private string name = "";

        //конструктор матрицы с указанием её размеров
        public Matrix(int rows, int columns, string name = "")
        {
            array = new int[rows, columns];
            this.name = name; 
        }

        //свойство получения кол-ва строк матрицы
        public int Rows
        {
            get { return array.GetLength(0); }
        }

        //свойство получения кол-ва столбцов матрицы
        public int Columns
        {
            get { return array.GetLength(1); }
        }

        //индексатор для доступа к элементам матрицы
        public int this[int i, int j]
        {
            get{ return array[i, j]; }
            set{ array[i, j] = value; }
        }

        //ввод
        public void InputMatrix()
        {
            int rows = Rows, columns = Columns;

            Console.Write("\nВведите кол-во строк матрицы " + name + ": ");
            rows = int.Parse(Console.ReadLine());
            Console.Write("\nВведите кол-во столбцов матрицы: " + name + ": ");
            columns = int.Parse(Console.ReadLine());

            array = new int[rows, columns];
            //Console.WriteLine("\nВведите матрицу " + name + ": ");
            for (int i = 0; i < rows; ++i)
            {
                Console.WriteLine("Введите элементы " + i + "-й строки: ");
                for (int j = 0; j < columns; ++j)
                {
                    Console.Write("Элемент[{0}][{1}] = ", i, j);
                    array[i, j] = int.Parse(Console.ReadLine());
                }
            }
        }

        //вывод
        public void OutputMatrix()
        {
            int rows = Rows, columns = Columns;

            Console.WriteLine("\nЗадана матрица " + name);

            for (int i = 0; i < rows; i++)
            { 
                for (int j = 0; j < columns;j++)
                {
                    Console.Write("{0, 3} ", array[i, j]);
                }
                Console.WriteLine();
            }

            Console.WriteLine();
        }

        //Min элемент
        /// <summary>
        /// Метод, что вычисляет минимальный элемент в матрице.
        /// </summary>
        /// <return>Результат - минимальный элемент</return>
        public int MinElement()
        {
            int min = array[0, 0];
            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    if (array[i, j] < min)
                    {
                        min = array[i, j];
                    }
                }
            }
            return min;
        }

        //Умножение на матрицы на число
        public static Matrix operator *(Matrix A, int integerNumber)
        {
            int rows = A.Rows, columns = A.Columns; 
            Matrix newMatrixD = new Matrix(rows, columns, "D");

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    newMatrixD[i, j] = A[i, j] * integerNumber;
                }
            }
            return newMatrixD;
        }

       //Умножения числа и матрицы
        public static Matrix operator *(int number, Matrix A)
        {
            return A * number;
        }

        //Кол-во отрицательных элементов
        /// <summary>
        /// Метод, что вычисляет кол-во отрицательных элементов .
        /// </summary>
        /// <return>Результат - Кол-во отрицательных элементов</return>
        public int NumberOfNegativeElements()
        {
            int negativeElements = array[0, 0]; 
            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    if (array[i, j] < 0)
                    {
                        negativeElements++;
                    }
                }
            }
            return negativeElements;
        }

        // <
        public static bool operator <(Matrix A, Matrix B)
        {
            if ((A.array.Length < B.array.Length) || ((A.array.Length == B.array.Length) && (A.NumberOfNegativeElements() > B.NumberOfNegativeElements())))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        // >
        public static bool operator >(Matrix A, Matrix B)
        {
            if ((A.array.Length > B.array.Length) || ((A.array.Length == B.array.Length) && (A.NumberOfNegativeElements() < B.NumberOfNegativeElements())))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        // =
        public static bool operator ==(Matrix A, Matrix B)
        {
            if ((A.array.Length == B.array.Length) && (A.NumberOfNegativeElements() == B.NumberOfNegativeElements()))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        // !=
        public static bool operator !=(Matrix A, Matrix B)
        {
            if ((A.array.Length != B.array.Length) && (A.NumberOfNegativeElements() == B.NumberOfNegativeElements()))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //Найти max элементы в каждой матрице 
        /// <summary>
        /// Метод, что вычисляет макс элемент в матрице.
        /// </summary>
        /// <return>Результат - макс элемент</return>
        public int MaxElement()
        {
            int max = array[0, 0]; 
            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    if (array[i, j] > max)
                    {
                        max = array[i, j];
                    }
                }
            }
            Console.WriteLine("Max элемент матрицы " + name + ": ");
            return max;
        }


        //Найти max элемент в матрице 5*А, среди элементов < 10 
        /// <summary>
        /// Метод, что вычисляет max элемент в матрице 5*А, среди элементов < presetNumber
        /// </summary>
        /// <param name="presetNumber">Передаваемое заранее заданное число (10)</param>
        /// <return>Результат - max элемент в матрице 5*А, среди элементов < prsetNumber</return>
        public int MaxElement(int presetNumber)
        {
            int max = MinElement();
            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    if (array[i, j] < presetNumber)
                    {
                        if (array[i, j] > max)
                        {
                            max = array[i, j];

                        }
                    }
                }
            }
            return max;
        }

        // поиск max элемента среди чётных и нечётных строк
        /// <summary>
        /// Метод, что вычисляет max элемента среди чётных или нечётных строк .
        /// </summary>
        /// <param name="evenNumber">Передаваемое параметр</param>
        /// <return>Результат - max элемент среди чётных и нечётных строк </return>
        public int MaxElement(int paramNumber, string name)
        {
            int maxElement = array[0, 0];

                for (int i = 0 + paramNumber; i < array.GetLength(0); i = i + 2)
                {
                    for (int j = 0; j < array.GetLength(1); j++)
                    {
                        if (array[i, j] > maxElement)
                        {
                            maxElement = array[i, j];
                        }
                    }
                }
                return maxElement;
        }

        //формирование нового одномерного массива из элементов матрицы А с определенными условиями
        /// <summary>
        /// Метод, что формирует новый одномерный массив из элементов матрицы А с определенными условиями
        /// </summary>
        /// <param name="A">Передаваемая матрица А</param>
        /// <param name="B">Передаваемая матрица В</param>
        /// <param name="С,">Передаваемая матрица С</param>

        public void FormationOfANewArray(Matrix A, Matrix B, Matrix C)
        {
                int count = 0;
                int[] newM = new int[C.array.Length];
                for(int i = 0; i < C.array.GetLength(0); i++)
                {
                    for(int j = 0; j < C.array.GetLength(1); j++)
                    {
                    if (C.array[i, j] > A.MaxElement(2, "A") && C.array[i, j] < B.MaxElement(1, "B"))
                    {
                            newM[count] = C.array[i, j];
                            count++;
                        }
                    }
                }
                Console.WriteLine();
                for (int i = 0; i < count; i++)
                {
                    Console.WriteLine(newM[i] + " ");
                }
                
                Console.ReadKey();

            //return array.Length;
        }
    }
}






