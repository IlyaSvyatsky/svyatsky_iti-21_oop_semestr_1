﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MatrixLib;

namespace OOPCSharp3
{
    class Program
    {
        static void Main(string[] args)
        {
            int choiceAction;
            int rows = 0;
            int columns = 0;

            Matrix A = new Matrix(rows, columns, "A");
            A.InputMatrix();

            Matrix B = new Matrix(rows, columns, "B");
            B.InputMatrix();

            Matrix C = new Matrix(rows, columns, "C");
            C.InputMatrix();
            
            A.OutputMatrix();
            B.OutputMatrix();
            C.OutputMatrix();

            Matrix D = new Matrix(rows, columns, "D");
            do
            {
                //меню

                Console.WriteLine("________________________________________________________________________");
                Console.WriteLine("|                                                                      |");
                Console.WriteLine("|                                   МЕНЮ                               |");
                Console.WriteLine("|______________________________________________________________________|");
                Console.WriteLine("|                                                                      |");
                Console.WriteLine("| 1. Вывести заданные матрицы                                          |");
                Console.WriteLine("| 2. Найти max элементы в каждой матрице                               |");
                Console.WriteLine("| 3. Найти max элемент в матрице 5*А, среди элементов < 10             |");                                                        
                Console.WriteLine("| 4. Если A>B>C, сформировать одномерный массив из элементов матрицы С |");
                Console.WriteLine("|    больших максимального среди элементов чётных строк матрицы А, и   |");                                
                Console.WriteLine("|    меньших максимального среди элементов нечтных строк матрицы В     |");
                Console.WriteLine("| 5. Выход из программы                                                |");
                Console.WriteLine("|______________________________________________________________________|");
                Console.WriteLine();

                Console.WriteLine();
                Console.WriteLine("Введите пункт меню (1-5): ");

                choiceAction = Convert.ToInt32(Console.ReadLine()); //ввод выбора

                switch (choiceAction)
                {
                    case 1:
                        A.OutputMatrix();
                        B.OutputMatrix();
                        C.OutputMatrix();
                        break;

                    case 2:
                        Console.WriteLine(A.MaxElement());
                        Console.WriteLine(B.MaxElement());
                        Console.WriteLine(C.MaxElement());
                        break;

                    case 3:
                        D = 5 * A;
                        D.OutputMatrix();
                        Console.WriteLine("min элемент в 5*A матрице, среди элементов < 10: " + D.MinElement());
                        if (D.MinElement() < 10)
                        {
                            Console.WriteLine("Наибольший элемент в 5*A матрице, среди элементов < 10: " + D.MaxElement(10));
                        }
                        else
                        {
                            Console.WriteLine("Наибольший элемент в 5*A матрице, среди элементов < 10 отсутствует! ");
                        }
                        break;

                    case 4:
                        if(A > B && A > C && B > C)
                        {
                            Console.WriteLine();
                            Console.Write("Формируем новый массив из элементов матрицы C больших " + A.MaxElement(2, "A") + " и меньших " + B.MaxElement(1, "B"));
                            Console.WriteLine();
                            /* A.FormationOfANewArray(A, B, C, "C");
                             B.FormationOfANewArray(A, B, C, "C");*/
                            C.FormationOfANewArray(A, B, C);
                        }
                        else
                        {
                            Console.Write("A = Матрица A, B = Матрица B, C = Матрица C");
                            Console.Write(" Результаты сравнения 3-х матриц: ");
                            Console.WriteLine();
                            if (A > B && A > C)
                            {
                                if (B == C)
                                {
                                    Console.WriteLine("B = C");
                                }
                                Console.WriteLine("A > B и A > C ");
                            }
                            if (B > A && B > C)
                            {
                                Console.WriteLine("B > A и B > C ");
                                if (A == C)
                                {
                                    Console.WriteLine("A = C ");
                                }
                            }
                            if (C > A && C > B)
                            {
                                Console.WriteLine("C > A и C > B ");
                                if (A == B)
                                {
                                    Console.WriteLine("A = B ");
                                }
                            }

                            if (A < B && A < C)
                            {
                                Console.WriteLine("A < B и A < C  ");
                                if (B == C)
                                {
                                    Console.WriteLine("B = C ");
                                }
                            }
                            if (B < A && B < C)
                            {
                                Console.WriteLine("B < A и B < C ");
                                if (A == C)
                                {
                                    Console.WriteLine("A = C ");
                                }
                            }
                            if (C < A && C < B)
                            {
                                Console.WriteLine("C < A и C < B ");
                                if (A == B)
                                {
                                    Console.WriteLine("A = B ");
                                }
                            }
                            if (A == B && A == C && B == C)
                            {
                                Console.WriteLine("A = B = C");
                            }
                        }
                        break;

                    default:
                        Console.WriteLine("Неверный пункт меню!");
                        break;
                }
            } while (choiceAction != 5);
        }
    }
}
