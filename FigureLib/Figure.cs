﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Пространство имен содержит все родительские / дочерние классы фигур.
/// </summary>
namespace FigureLib
{

    /// <summary>
    /// Родительский класс для всех фигур.
    /// </summary>
    /// <returns></returns>

    public abstract class Figure
    {

        /// <summary>
        /// Метод, который считает периметр фигуры.
        /// </summary>
        /// <returns>Периметр фигуры.</returns>
        public abstract Double PerimetrFigure();

        /// <summary>
        /// Метод, который считает площадь фигуры.
        /// </summary>
        /// <returns>Площадь фигуры.</returns>
        public abstract Double AreaFigure();

        /// <summary>
        /// Метод, который проверяет принадлежность точки фигуре.
        /// </summary>
        /// <param name="X">Координата X проверяемой точки.</param>
        /// <param name="Y">Координаты Y проверяемой точки.</param>
        /// <returns>Строка расположения проверяемой точки.</returns>
        public abstract String PointAffiliation(double X, double Y);
    }
}
