﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

///<summary>
/// Пространство имен содержит все родительские / дочерние классы фигур.
/// </summary>
namespace FigureLib
{

    /// <summary>
    /// Дочерний класс, который содержит методы для вычисления фигуры.
    /// </summary>

    public class Trapezium : Figure
    {
        /// <summary>
        /// Метод, который контролирует ввод для поля startIntervall.
        /// </summary>

        public double StartInterval { get; private set; }

        /// <summary>
        /// Метод, который контролирует ввод для поля endInterval.
        /// </summary>

        public double EndInterval { get; private set; }

        ///<summary>
        ///Конструктор, который инициализирует поля класса.
        /// </summary>

        /// <param name="StartInterval">Начальный интервал фигуры.</param>
        /// <param name="EndInterval">Конечный интервал фигуры.</param>

        public Trapezium(double StartInterval, double EndInterval)
        {
            this.StartInterval = StartInterval;
            this.EndInterval = EndInterval;
        }

        /// <summary>
        /// Метод проверияет существование фигуры.
        /// </summary>
        /// <returns>Данные проверки существования фигуры.</returns>

        public bool CorrectFigure()
        {
            return ((Math.Sin(StartInterval) <= 0 && Math.Sin(EndInterval) <= 0) || (Math.Sin(StartInterval) >= 0 && Math.Sin(EndInterval) >= 0));
        }

        /// <summary>
        /// Метод, что вычисляет длину стороны фигуры, образованной начальным интервалом.
        /// </summary>
        /// <return>Длина стороны А. </return>

        public double SideA()
        {
            return Math.Abs(Math.Sin(StartInterval));
        }

        /// <summary>
        /// Метод, что вычисляет длину стороны фигуры, образованной конечным интервалом.
        /// </summary>
        /// <return>Длина стороны В. </return>

        public double SideB()
        {
            return Math.Abs(Math.Sin(EndInterval));
        }

        /// <summary>
        /// Метод, что вычисляет длину стороны основания фигуры.
        /// </summary>
        /// <return>Длина стороны С. </return>

        public double SideC()
        {
            return Math.Abs(EndInterval - StartInterval);
        }

        /// <summary>
        /// Метод, который считает длину дуги фигуры.
        /// </summary>
        /// <returns>Длину стороны D.</returns>

        public double SideD()
        {
            //длина дуги = опред. интеграл (корень(1 + производная (sin)') = опред. интеграл (корень(1 + cos(x)^2))

            int iterations; //число отрезков на которые разбивается[a, b]
            double L; //длина дуги фигуры
            double step; // шаг разбиения

            iterations = 1000000; // чем больше, тем лучше

            //длина каждого из маленьких отрезков или шаг разбиения
            step = (EndInterval - StartInterval) / iterations;
            //сумма первого и последнего значения подынтегральной функции
            L = 0.5 * (Math.Sqrt(1 + Math.Pow(Math.Cos(StartInterval), 2)) + Math.Sqrt(1 + Math.Pow(Math.Cos(EndInterval), 2)));

            for (int i = 0; i < iterations; ++i) //цикл перебора участков разбиения
            {
                L += (Math.Sqrt(1 + Math.Pow(Math.Cos(StartInterval + step * i), 2))); //считаем сам интеграл
            }
            L *= step; //конечный результат
            return L;
        }

        /// <summary>
        /// Метод, который считает периметр фигуры.
        /// </summary>
        /// <returns>Периметр фигуры.</returns>

        public override double PerimetrFigure()//свойство для нахождения периметра
        {
            return (SideA() + SideB() + SideC() + SideD()) ;
        }

        /// <summary>
        /// Метод подинтегральной функции y = |sin(x)|.
        /// </summary>
        /// <param name="x"></param>
        /// <returns>y = |sin(x)|</returns>

        private double Function(double x)
        {
            return Math.Abs(Math.Sin(x));
        }

        /// <summary>
        /// Метод, который считает площадь фигуры.
        /// </summary>
        /// <returns>Площадь фигуры.</returns>

        public override double AreaFigure()
        {
            //площадь синусоиды есть решение её определенного интеграла
            //определенный интеграл от sin(x) = cos(EndInterval) - cos(StartInterval)

            int iterations; //число отрезков на которые разбивается[a, b]
            double S; //длина дуги фигуры
            double step; // шаг разбиения

            iterations = 1000000; // чем больше, тем лучше

            //длина каждого из маленьких отрезков или шаг разбиения
            step = (EndInterval - StartInterval) / iterations;
            //сумма первого и последнего значения подынтегральной функции
            S = 0.5 * Function(StartInterval) + Function(EndInterval);

            for (int i = 0; i < iterations; ++i) //цикл перебора участков разбиения
            {
                S += Function(StartInterval + step * i); //считаем сам интеграл
            }
            S *= step; //конечный результат
            return S;
        }

        /// <summary>
        /// Метод, который проверяет принадлежность точки фигуре.
        /// </summary>
        /// <param name="X">Координата X проверяемой точки.</param>
        /// <param name="Y">Координаты Y проверяемой точки.</param>
        /// <returns>Строка расположения проверяемой точки.</returns>

        public override string PointAffiliation(double X, double Y)
        {
            string location;

            if ((X > 0 && Y > 0 && Y < Math.Sin(X) && X < EndInterval) || (X > 0 && Y < 0 && Y > Math.Sin(X) && X < EndInterval) || (X < 0 && Y < 0 && Y > Math.Sin(X) && X > StartInterval) || (X < 0 && Y > 0 && Y < Math.Sin(X) && X > StartInterval))
            {
                location = "Точка лежит внутри фигуры.";
            }
            else
            if ((X == StartInterval && Y >= Math.Sin(X) && Y <= 0) || (X == StartInterval && Y <= Math.Sin(X) && Y >= 0) || (X == EndInterval && Y <= Math.Sin(X) && Y >= 0) || (X == EndInterval && Y >= Math.Sin(X) && Y <= 0) || (Y == 0 && X >= StartInterval && X <= EndInterval))
            {
                location = "Точка лежит на границе фигуры.";
            }
            else
            {
                location = "Точка не принадлежит фигуре.";
            }

            return location;
        }

    }
}

