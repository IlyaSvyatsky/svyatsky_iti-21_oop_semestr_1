﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using HotDogLib;

namespace OOPCSharp7
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private HotDogRecords hotDogRecords;

        public MainWindow()
        {
            InitializeComponent();
            this.hotDogRecords = new HotDogRecords();
        }

        public void UpdateTable()
        {
            int i = 0;
            List<HotDog> table = new List<HotDog>();
            for (i = 0; i < hotDogRecords.Length; i++)
            {
                table.Add(new HotDog()
                {
                    TypeOfSausage = hotDogRecords.arrayOfHotDogs[i].TypeOfSausage,
                    TypeOfBun = hotDogRecords.arrayOfHotDogs[i].TypeOfBun,
                    TypeOfSauce = hotDogRecords.arrayOfHotDogs[i].TypeOfSauce,
                    Cost = hotDogRecords.arrayOfHotDogs[i].Cost,
                });
            }
            HotDogTable.Items.Refresh();
            HotDogTable.AutoGenerateColumns = false;
            HotDogTable.ItemsSource = table;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string typeOfSausageRecord = "";
            string typeOfBunRecord = "";
            string typeOfSauceRecord = "";
            double costRecord = Convert.ToDouble(Text4.Text);
            typeOfSausageRecord = Text1.Text;
            typeOfBunRecord = Text2.Text;
            typeOfSauceRecord = Text3.Text;
            hotDogRecords.AddHotDog(typeOfSausageRecord, typeOfBunRecord, typeOfSauceRecord, costRecord);
            UpdateTable();
            //this.Close();
        }

        private void Button1_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Общая сумма всех заказов: " + hotDogRecords.SumOfAllOrders());
        }

        private void Button2_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Количество полных заказов: " + hotDogRecords.CountOfFullOrders());
        }

        private void Button3_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Средняя стоимость только заказов с булочкой: " + hotDogRecords.AverageOrderValueWithABunOnly());
        }
    }
}
