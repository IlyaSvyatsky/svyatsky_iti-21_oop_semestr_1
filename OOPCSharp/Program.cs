﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FigureLib;

namespace OOPSharp
{
    /// <summary>
    /// Главный класс этой программы.
    /// </summary>

    class Program
    {

        /// <summary>
        /// Главный метод программы.
        /// </summary>
        /// <param name="args">Стандартный основной параметр.</param>

        static void Main(string[] args)
        {
            //Обьявление переменных

            double startInterval; //начальный интервал
            double endInterval; //конечный интервал

            int choiceMenu; // выбор в меню

            Trapezium trapezium;

            do
            {
                //ввод начальных значений интервалов
                Console.WriteLine("\nВведите начальное значение интервала: ");
                startInterval = Convert.ToDouble(Console.ReadLine());

                Console.WriteLine("\nВведите конечное значение интервала: ");
                endInterval = Convert.ToDouble(Console.ReadLine());

                trapezium = new Trapezium(startInterval, endInterval); //создание нового обьекта с параметрами()

                if (trapezium.CorrectFigure()) //проверка на сушествование фигуры
                {
                    if (endInterval > startInterval) //проверка на правильность ввода интервалов и сушествования фигуры
                    {
                        Console.WriteLine("Фигура существует и успешно создана!");
                    }
                }
                else
                {
                    Console.WriteLine(" Фигура не существует! Неправильно заданы интервалы!");
                }
            } while (trapezium.CorrectFigure() != true);

            do
            {
                //меню

                Console.WriteLine("________________________________________________________");
                Console.WriteLine("|                                                      |");
                Console.WriteLine("|                          МЕНЮ                        |");
                Console.WriteLine("|______________________________________________________|");
                Console.WriteLine("|                                                      |");
                Console.WriteLine("| Криволинейная трапеция на заданном интеравале, обра- |");
                Console.WriteLine("| зуемая функцией sin(x) и осью OX.                    |");
                Console.WriteLine("|                                                      |");
                Console.WriteLine("| 1. Вычислить периметр криволинейной трапеции.        |");
                Console.WriteLine("| 2. Вычислить площадь криволиннйной трапеции.         |");
                Console.WriteLine("| 3. Проверить принадлежность точки, заданной своими   |");
                Console.WriteLine("|    координатами на плоскости, фигуре и её границе.   |");
                Console.WriteLine("| 4. Выход из программы.                               |");
                Console.WriteLine("|______________________________________________________|");
                Console.WriteLine();
                Console.WriteLine("Введите пункт меню (1-4): ");

                choiceMenu = Convert.ToInt32(Console.ReadLine()); //ввод выбора

                switch (choiceMenu) //оператор выбора
                {
                    case 1:
                        //вызов метода, который считает длину  фигуры и выводит результат
                        Console.WriteLine(" Периметр криволинейной трапеции P = " + trapezium.PerimetrFigure());
                        Console.WriteLine(" Сторона A = " + trapezium.SideA());
                        Console.WriteLine(" Сторона B = " + trapezium.SideB());
                        Console.WriteLine(" Сторона C = " + trapezium.SideC());
                        Console.WriteLine(" Сторона D = " + trapezium.SideD());
                        break;

                    case 2:
                        //вызов метода, который считает площадь фигуры и выводит результат
                        Console.WriteLine(" Площадь криволинейной трапеции S = " + trapezium.AreaFigure());
                        break;

                    case 3:
                        double x; //координата x
                        double y; //координата y

                        Console.WriteLine("\nВведите координату X: ");
                        x = Convert.ToDouble(Console.ReadLine());

                        Console.WriteLine("\nВведите координату Y: ");
                        y = Convert.ToDouble(Console.ReadLine());

                        //вызов метода, который проверяет принадлежность точки и выводит результат
                        Console.WriteLine(trapezium.PointAffiliation(x, y));
                        break;

                    default:
                        Console.WriteLine(" Неверный пункт меню!");
                        break;
                }

            } while (choiceMenu != 4);
        }
    }
}
