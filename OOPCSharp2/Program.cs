﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PolynomialLib;

namespace OOPCSharp2
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                int choiceAction;
                int degree = 0; //степень полинома

                Console.Write("Введите степень первого полинома: ");
                degree = int.Parse(Console.ReadLine());
                Polynom a = new Polynom(degree + 1);
                for (int i = 0; i <= degree; i++) //int i = degree; i >= 0; i--)
                {
                    Console.Write("Введите коэффициент при степени " + i + ":");
                    a[i] = double.Parse(Console.ReadLine());
                }
                Console.WriteLine();
                Console.Write("Введите степень второго полинома: ");
                degree = int.Parse(Console.ReadLine());
                Polynom b = new Polynom(degree + 1);
                for (int i = 0; i <= degree; i++)//int i = degree; i >= 0; i--)
                {
                    Console.Write("Введите коэффициент при степени " + i + ":");
                    b[i] = double.Parse(Console.ReadLine());
                }

                // A.InputPolynomial("A");

                //B.InputPolynomial("B");

                Console.WriteLine();
                Console.WriteLine(a.SPolynom());
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine(b.SPolynom());

                Polynom c = new Polynom(0);

                do
                {
                    //меню

                    Console.WriteLine("________________________________________________________");
                    Console.WriteLine("|                                                      |");
                    Console.WriteLine("|                          МЕНЮ                        |");
                    Console.WriteLine("|______________________________________________________|");
                    Console.WriteLine("|                                                      |");
                    Console.WriteLine("|                  Полином (многочлен)                 |");
                    Console.WriteLine("|                                                      |");
                    Console.WriteLine("| 1. Вычислить сумму полиномов                         |");
                    Console.WriteLine("| 2. Вычислить произведение полинома на число          |");
                    Console.WriteLine("| 3. Выход из программы.                               |");
                    Console.WriteLine("|______________________________________________________|");
                    Console.WriteLine();

                    Console.WriteLine();
                    Console.WriteLine("Введите пункт меню (1-3): ");

                    choiceAction = Convert.ToInt32(Console.ReadLine()); //ввод выбора

                    switch (choiceAction)
                    {
                        case 1:
                            c = a + b;
                            Console.WriteLine(" Результат  операции сложения: " + c.SPolynom());
                            break;

                        case 2:
                            Console.WriteLine("Введите число: ");
                            double number = double.Parse(Console.ReadLine());

                            c = a * number;
                            Console.WriteLine(" Результат  операции умножения на число: " + c.SPolynom());
                            break;

                        default:
                            Console.WriteLine("Неверный пункт меню!");
                            break;
                    }

                } while (choiceAction != 4);
            }
        }

    }
}


/*Polynom A = new Polynom(degree);
Console.WriteLine("Введите степень первого полинома: ");
            degree = int.Parse(Console.ReadLine());

            for (int i = degree; i >= 0; i--)
            {
                Console.Write("Введите коэффициент при степени " + i + "(дробное):");
                array[i] = double.Parse(Console.ReadLine());
            }

            Console.WriteLine(A.SPolynom);

            Polynom B = new Polynom(degree);
Console.WriteLine("Введите степень второго полинома: ");
            degree = int.Parse(Console.ReadLine()); //Convert.ToInt32(Console.ReadLine());
            for (int i = degree; i >= 0; i--)
            {
                Console.Write("Введите коэффициент при степени " + i + "(дробное):");
                array[i] = double.Parse(Console.ReadLine());
            }

            Console.WriteLine(B.SPolynom);
            Polynom C = new Polynom(0);*/