﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaloriesLib
{
    public class Calories
    {
        /// <summary>
        /// Массив записей.
        /// </summary>
        private Record[] arrayOfRecords;

        /// <summary>
        /// Количество записей.
        /// </summary>
        private Int32 numberOfRecords;

        /// <summary>
        /// Запись класса.
        /// </summary>
        private class Record
        {
            /// <summary>
            /// Название продукта.
            /// </summary>
            public String Calories { get; private set; }

            /// <summary>
            /// Название продукта.
            /// </summary>
            public String Food { get; private set; }

            /// <summary>
            /// Дата записи.
            /// </summary>
            public DateTime Date { get; private set; }

            /// <summary>
            /// Индекс специальности.
            /// </summary>
            public Int32 specialtyIndex;

            /// <summary>
            /// Record constructor. Запись конструктора.
            /// </summary>
            /// <param name="patient">patient</param>
            /// <param name="food">doctor</param>
            /// <param name="date">record</param>
            /// <param name="specialtyIndex">index specialty</param>
            public Record(String calories, String food, DateTime date, Int32 specialtyIndex = 0)
            {
                this.Food = food != null ? food : "food";
                this.Calories = calories != null ? calories : "calories";
                this.Date = date != null ? date : new DateTime();
                this.specialtyIndex = specialtyIndex;
            }

            /// <summary>
            /// Get string represent specialty. Получить строку представляют специальности.
            /// </summary>
            /// <returns>Строка представляет специальность.</returns>
            public String GetSpecialtyString()
            {
                switch (specialtyIndex)
                {
                    case 0:
                        return "Фрукты";
                    case 1:
                        return "Злаки";
                    case 2:
                        return "Овощи";
                    case 3:
                        return "Травы и пряности";
                    case 4:
                        return "Соки";
                    case 5:
                        return "Мясо";
                    case 6:
                        return "Молочное";
                    case 7:
                        return "Яйца";
                    case 8:
                        return "Моллюски и ракообразные";
                    case 9:
                        return "Насекомые";
                    case 10:
                        return "Грибы";
                    case 11:
                        return "Прочее органическое";

                    default: return "what?";
                }
            }

            /// <summary>
            /// Get string represent specialty.Получить строку представляют специальности.
            /// </summary>
            /// <param name="specialtyIndex">Index specialty</param>
            /// <returns>String represent specialty.</returns>
            public static String GetSpecialtyString(Int32 specialtyIndex)
            {
                switch (specialtyIndex)
                {
                    case 0:
                        return "Фрукты";
                    case 1:
                        return "Злаки";
                    case 2:
                        return "Овощи";
                    case 3:
                        return "Травы и пряности";
                    case 4:
                        return "Соки";
                    case 5:
                        return "Мясо";
                    case 6:
                        return "Молочное";
                    case 7:
                        return "Яйца";
                    case 8:
                        return "Моллюски и ракообразные";
                    case 9:
                        return "Насекомые";
                    case 10:
                        return "Грибы";
                    case 11:
                        return "Прочее органическое";

                    default: return "what?";
                }
            }

            /// <summary>
            /// Override ToString.
            /// </summary>
            /// <returns>Строковый объект.</returns>
            public override String ToString()
            {
                String answer = "";
                answer += Calories + " " + Date.ToShortDateString() + " " + GetSpecialtyString() + " " + Food;
                return answer;
            }

            /*/// <summary>
            /// Override ToString.
            /// </summary>
            /// <param name="obj">Check object.</param>
            /// <returns>True if objects are equals. Истинно, если объекты равны.</returns>
            public override Boolean Equals(object obj)
            {
                if (obj == null || obj.GetType() != this.GetType()) return false;
                Record recordObj = (Record)obj;
                if ((this.Calories == recordObj.Calories) && (this.Food == recordObj.Food) &&
                    (this.Date == recordObj.Date) && (this.specialtyIndex == recordObj.specialtyIndex))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            
            /// <summary>
            /// Override GetHashCode.
            /// </summary>
            /// <returns>HashCode.</returns>
            public override Int32 GetHashCode()
            {
                return Calories.GetHashCode() ^ Food.GetHashCode() ^ Date.GetHashCode() ^ specialtyIndex.GetHashCode();
            }*/
        }

        /// <summary>
        /// Конструктор...
        /// </summary>
        public Calories()
        {
            arrayOfRecords = new Record[1000];
            numberOfRecords = 0;
        }

        /// <summary>
        /// Добавить запись в массив записей.
        /// </summary>
        /// <param name="calories"></param>
        /// <param name="food"></param>
        /// <param name="date"></param>
        /// <param name="specialtyIndex"></param>
        public void AddMedicalRecord(String calories, String food, DateTime date, Int32 specialtyIndex)
        {
            arrayOfRecords[numberOfRecords] = new Record(calories, food, date, specialtyIndex);
            numberOfRecords++;
        }

        /// <summary>
        /// Return array of doctors in day. Вернет массив врачей за день.
        /// </summary>
        /// <param name="date">Check date. Дата проверки</param>
        /// <returns>Array of doctors. Массив врачей.</returns>
        private String[] GetDoctors(DateTime date)
        {
            String[] products = new String[numberOfRecords];
            Int32 amountDoctors = 0;
            for (Int32 i = 0; i < numberOfRecords; i++)
            {
                if (arrayOfRecords[i].Date.Date == date.Date)
                {
                   /* Boolean doctorIs = false;
                    for (Int32 j = 0; j < amountDoctors; j++)
                    {
                        doctorIs = String.Equals(products[j], arrayOfRecords[i].Food) ? true : doctorIs;
                    }*/
                    //if (!doctorIs)
                    //{
                        products[amountDoctors] = arrayOfRecords[i].Food;
                        amountDoctors++;
                    //}
                }
            }
            return (String[])products.Clone();
        }

        /// <summary>
        /// Return doctors in day. Верните врачей в день. //выводит список еды за день
        /// </summary>
        /// <param name="date">Check date.</param>
        /// <returns>String doctors.</returns>
        public String DoctorsInDay(DateTime date)
        {
            String[] products = GetDoctors(date);
            String answer = "";
            for (Int32 i = 0; i < products.Length; i++)
            {
                /*if (products[i] != null) */answer += products[i] + "\n";
            }
            return answer;
        }

        /// <summary>
        /// Return index of value in array. Возвращает индекс значения в массиве.
        /// </summary>
        /// <param name="array">Array.</param>
        /// <param name="value">Value.</param>
        /// <returns>Index. </returns>
        private Int32 GetIndexStringArray(String[] array, String value)
        {
            for (Int32 i = 0; i < array.Length; i++)
            {
                if (String.Equals(value, array[i])) return i;
            }
            return array.Length;
        }

        /// <summary>
        /// Return amount patient at doctor. Верните сумму пациенту у врача.
        /// </summary>
        /// <param name="date">Check date.</param>
        /// <returns>String answer.</returns>
        public String AmountPatients(DateTime date)
        {
            string result = "";
            double answer = 0;
            String[] products = GetDoctors(date);
            Int32[] amountPatints = new Int32[numberOfRecords];
            for (Int32 i = 0; i < numberOfRecords; i++)
            {
                if (arrayOfRecords[i].Date.Date == date.Date)
                {
                    answer = answer + amountPatints[i];
                    //answer = answer + products[i];
                    //amountPatints[GetIndexStringArray(products, arrayOfRecords[i].Food)]++;
                }
            }
            
            for (Int32 i = 0; i < products.Length; i++)
            {
                
                 //answer +=" " + products[i] + " " + amountPatints[i] + "\n";
                result = " количество употреблённых калорий в день: " + Convert.ToString(answer);
            }
            return result;
        }

       /* public String AverageAmountRecordsForSpecialty()
        {
            String answer = "";
            Int32[] specialtys = GetSpecialty();
            Int32[,] potientsAtDay = new Int32[specialtys.Length, 2];
            for (Int32 i = 0; i < numberOfRecords; i++)
            {
                Int32 j = 0;
                while (specialtys[j] != arrayOfRecords[i].specialtyIndex) j++;
                potientsAtDay[j, 0]++;
                if (newDay(i, specialtys[j]))
                {
                    potientsAtDay[j, 1]++;
                }
            }
            for (Int32 i = 0; i < specialtys.Length; i++)
            {
                if (specialtys[i] != -2) answer += Record.GetSpecialtyString(specialtys[i]) + " " + (Single)potientsAtDay[i, 0] / potientsAtDay[i, 1] + "\n";
            }
            return answer;
        }

        /// <summary>
        /// Get all used specialty. Получить всю использованную специальность.
        /// </summary>
        /// <returns>Array specialty.</returns>
        private Int32[] GetSpecialty()
        {
            Int32[] specialtys = new Int32[9] { -2, -2, -2, -2, -2, -2, -2, -2, -2 };
            Int32 amountGetSpecialty = 0;
            for (Int32 i = 0; i < numberOfRecords; i++)
            {
                Boolean newSpecialty = true;
                for (Int32 j = 0; j < amountGetSpecialty; j++)
                {
                    if (specialtys[j] == arrayOfRecords[i].specialtyIndex) newSpecialty = false;
                }
                if (newSpecialty)
                {
                    specialtys[amountGetSpecialty] = arrayOfRecords[i].specialtyIndex;
                    amountGetSpecialty++;
                }
            }
            return specialtys;
        }
        /// <summary>
        /// Is this a new day. Это новый день.
        /// </summary>
        /// <param name="recordsIndex">Index record. Индексная запись.</param>
        /// <param name="specialtyIndex">Index specialty. Index specialty</param>
        /// <returns>Bool answer.</returns>
        private Boolean newDay(Int32 recordsIndex, Int32 specialtyIndex)
        {
            Boolean answer = true;
            for (Int32 i = recordsIndex - 1; i >= 0; i--)
            {
                if ((arrayOfRecords[recordsIndex].Date.Date == arrayOfRecords[i].Date.Date) && (specialtyIndex == arrayOfRecords[i].specialtyIndex))
                    answer = false;
            }
            return answer;
        }
        /// <summary>
        /// Average amount records for specialty. Среднее количество записей по специальности.
        /// </summary>
        /// <returns>String answer.</returns>
        public String AverageAmountRecordsForSpecialty()
        {
            String answer = "";
            Int32[] specialtys = GetSpecialty();
            Int32[,] potientsAtDay = new Int32[specialtys.Length, 2];
            for (Int32 i = 0; i < numberOfRecords; i++)
            {
                Int32 j = 0;
                while (specialtys[j] != arrayOfRecords[i].specialtyIndex) j++;
                potientsAtDay[j, 0]++;
                if (newDay(i, specialtys[j]))
                {
                    potientsAtDay[j, 1]++;
                }
            }
            for (Int32 i = 0; i < specialtys.Length; i++)
            {
                if (specialtys[i] != -2) answer += Record.GetSpecialtyString(specialtys[i]) + " " + (Single)potientsAtDay[i, 0] / potientsAtDay[i, 1] + "\n";
            }
            return answer;
        }
        /// <summary>
        /// Override ToString.
        /// </summary>
        /// <returns>String object</returns>
        public override String ToString()
        {
            String answer = "";
            for (int i = 0; i < numberOfRecords; i++)
            {
                answer += arrayOfRecords[i].ToString() + "\n";
            }
            return answer;
        }
        /// <summary>
        /// Override Equals.
        /// </summary>
        /// <param name="obj">Check object.</param>
        /// <returns>True if are equal.</returns>
        public override Boolean Equals(object obj)
        {
            if (obj == null || obj.GetType() != this.GetType()) return false;
            Calories medicalRecordsObj = (Calories)obj;
            if (this.numberOfRecords != medicalRecordsObj.numberOfRecords) return false;
            for (int i = 0; i < medicalRecordsObj.numberOfRecords; i++)
            {
                if (!Record.Equals(this.arrayOfRecords[i], medicalRecordsObj.arrayOfRecords[i])) return false;
            }
            return true;
        }
        /// <summary>
        /// Override GetHashCode.
        /// </summary>
        /// <returns>HashCode</returns>
        public override Int32 GetHashCode()
        {
            return arrayOfRecords.GetHashCode() ^ numberOfRecords.GetHashCode();
        }*/



        /*/// <summary>
        /// Массив записей.
        /// </summary>
        private Record[] records;
        /// <summary>
        /// Количество записей.
        /// </summary>
        private Int32 amountRecords;
        /// <summary>
        /// Запись класса.
        /// </summary>
        private class Record
        {
            /// <summary>
            /// Название продукта.
            /// </summary>
            public String Patient { get; private set; }
            /// <summary>
            /// Название продукта.
            /// </summary>
            public String Food { get; private set; }
            /// <summary>
            /// Дата записи.
            /// </summary>
            public DateTime Date { get; private set; }
            /// <summary>
            /// Индекс специальности.
            /// </summary>
            public Int32 specialtyIndex;
            /// <summary>
            /// Record constructor. Запись конструктора.
            /// </summary>
            /// <param name="patient">patient</param>
            /// <param name="food">doctor</param>
            /// <param name="date">record</param>
            /// <param name="specialtyIndex">index specialty</param>
            public Record(String patient, String doctor, DateTime date, Int32 specialtyIndex = 0)
            {
                this.Food = food != null ? food : "food";
                this.Patient = patient != null ? patient : "patient";
                this.Date = date != null ? date : new DateTime();
                this.specialtyIndex = specialtyIndex;
            }
            /// <summary>
            /// Get string represent specialty. Получить строку представляют специальности.
            /// </summary>
            /// <returns>Строка представляет специальность.</returns>
            public String GetSpecialtyString()
            {
                switch (specialtyIndex)
                {
                    case 0: return "Педиатр";
                    case 1: return "Офтальмолог";
                    case 2: return "Хирург";
                    case 3: return "Невролог";
                    case 4: return "Стоматолог";
                    case 5: return "Терапевт";
                    case 6: return "Психолог";
                    case 7: return "Кардиолог";
                    default: return "?????";
                }
            }
            /// <summary>
            /// Get string represent specialty.Получить строку представляют специальности.
            /// </summary>
            /// <param name="specialtyIndex">Index specialty</param>
            /// <returns>String represent specialty.</returns>
            public static String GetSpecialtyString(Int32 specialtyIndex)
            {
                switch (specialtyIndex)
                {
                    case 0: return "Педиатр";
                    case 1: return "Офтальмолог";
                    case 2: return "Хирург";
                    case 3: return "Невролог";
                    case 4: return "Стоматолог";
                    case 5: return "Терапевт";
                    case 6: return "Психолог";
                    case 7: return "Кардиолог";
                    default: return "?????";
                }
            }
            /// <summary>
            /// Override ToString.
            /// </summary>
            /// <returns>Строковый объект.</returns>
            public override String ToString()
            {
                String answer = "";
                answer += Patient + " " + Date.ToShortDateString() + " " + GetSpecialtyString() + " " + Doctor;
                return answer;
            }
            /// <summary>
            /// Override ToString.
            /// </summary>
            /// <param name="obj">Check object.</param>
            /// <returns>True if objects are equals. Истинно, если объекты равны.</returns>
            public override Boolean Equals(object obj)
            {
                if (obj == null || obj.GetType() != this.GetType()) return false;
                Record recordObj = (Record)obj;
                if ((this.Patient == recordObj.Patient) && (this.Doctor == recordObj.Doctor) &&
                    (this.Date == recordObj.Date) && (this.specialtyIndex == recordObj.specialtyIndex))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            /// <summary>
            /// Override GetHashCode.
            /// </summary>
            /// <returns>HashCode.</returns>
            public override Int32 GetHashCode()
            {
                return Patient.GetHashCode() ^ Doctor.GetHashCode() ^ Date.GetHashCode() ^ specialtyIndex.GetHashCode();
            }
        }
        /// <summary>
        /// MedicalRecords constructor.
        /// </summary>
        public MedicalRecords()
        {
            records = new Record[1000];
            amountRecords = 0;
        }
        /// <summary>
        /// Добавить запись в массив записей.
        /// </summary>
        /// <param name="patient"></param>
        /// <param name="doctor"></param>
        /// <param name="date"></param>
        /// <param name="specialtyIndex"></param>
        public void AddMedicalRecord(String patient, String doctor, DateTime date, Int32 specialtyIndex)
        {
            records[amountRecords] = new Record(patient, doctor, date, specialtyIndex);
            amountRecords++;
        }
        /// <summary>
        /// Return array of doctors in day. Верните множество врачей в день.
        /// </summary>
        /// <param name="date">Check date. Дата проверки</param>
        /// <returns>Array of doctors. Массив врачей.</returns>
        private String[] GetDoctors(DateTime date)
        {
            String[] doctors = new String[amountRecords];
            Int32 amountDoctors = 0;
            for (Int32 i = 0; i < amountRecords; i++)
            {
                if (records[i].Date.Date == date.Date)
                {
                    Boolean doctorIs = false;
                    for (Int32 j = 0; j < amountDoctors; j++)
                    {
                        doctorIs = String.Equals(doctors[j], records[i].Doctor) ? true : doctorIs;
                    }
                    if (!doctorIs)
                    {
                        doctors[amountDoctors] = records[i].Doctor;
                        amountDoctors++;
                    }
                }
            }
            return (String[])doctors.Clone();
        }
        /// <summary>
        /// Return doctors in day. Верните врачей в день.
        /// </summary>
        /// <param name="date">Check date.</param>
        /// <returns>String doctors.</returns>
        public String DoctorsInDay(DateTime date)
        {
            String[] doctors = GetDoctors(date);
            String answer = "";
            for (Int32 i = 0; i < doctors.Length; i++)
            {
                if (doctors[i] != null) answer += doctors[i] + "\n";
            }
            return answer;
        }
        /// <summary>
        /// Return index of value in array. Возвращает индекс значения в массиве.
        /// </summary>
        /// <param name="array">Array.</param>
        /// <param name="value">Value.</param>
        /// <returns>Index. </returns>
        private Int32 GetIndexStringArray(String[] array, String value)
        {
            for (Int32 i = 0; i < array.Length; i++)
            {
                if (String.Equals(value, array[i])) return i;
            }
            return array.Length;
        }
        /// <summary>
        /// Return amount patient at doctor. Верните сумму пациенту у врача.
        /// </summary>
        /// <param name="date">Check date.</param>
        /// <returns>String answer.</returns>
        public String AmountPatients(DateTime date)
        {
            String answer = "";
            String[] doctors = GetDoctors(date);
            Int32[] amountPatints = new Int32[amountRecords];
            for (Int32 i = 0; i < amountRecords; i++)
            {
                if (records[i].Date.Date == date.Date)
                {
                    amountPatints[GetIndexStringArray(doctors, records[i].Doctor)]++;
                }
            }
            for (Int32 i = 0; i < doctors.Length; i++)
            {
                if (doctors[i] != null) answer += doctors[i] + " " + amountPatints[i] + "\n";
            }
            return answer;
        }
        /// <summary>
        /// Get all used specialty. Получить всю использованную специальность.
        /// </summary>
        /// <returns>Array specialty.</returns>
        private Int32[] GetSpecialty()
        {
            Int32[] specialtys = new Int32[9] { -2, -2, -2, -2, -2, -2, -2, -2, -2 };
            Int32 amountGetSpecialty = 0;
            for (Int32 i = 0; i < amountRecords; i++)
            {
                Boolean newSpecialty = true;
                for (Int32 j = 0; j < amountGetSpecialty; j++)
                {
                    if (specialtys[j] == records[i].specialtyIndex) newSpecialty = false;
                }
                if (newSpecialty)
                {
                    specialtys[amountGetSpecialty] = records[i].specialtyIndex;
                    amountGetSpecialty++;
                }
            }
            return specialtys;
        }
        /// <summary>
        /// Is this a new day. Это новый день.
        /// </summary>
        /// <param name="recordsIndex">Index record. Индексная запись.</param>
        /// <param name="specialtyIndex">Index specialty. Index specialty</param>
        /// <returns>Bool answer.</returns>
        private Boolean newDay(Int32 recordsIndex, Int32 specialtyIndex)
        {
            Boolean answer = true;
            for (Int32 i = recordsIndex - 1; i >= 0; i--)
            {
                if ((records[recordsIndex].Date.Date == records[i].Date.Date) && (specialtyIndex == records[i].specialtyIndex))
                    answer = false;
            }
            return answer;
        }
        /// <summary>
        /// Average amount records for specialty. Среднее количество записей по специальности.
        /// </summary>
        /// <returns>String answer.</returns>
        public String AverageAmountRecordsForSpecialty()
        {
            String answer = "";
            Int32[] specialtys = GetSpecialty();
            Int32[,] potientsAtDay = new Int32[specialtys.Length, 2];
            for (Int32 i = 0; i < amountRecords; i++)
            {
                Int32 j = 0;
                while (specialtys[j] != records[i].specialtyIndex) j++;
                potientsAtDay[j, 0]++;
                if (newDay(i, specialtys[j]))
                {
                    potientsAtDay[j, 1]++;
                }
            }
            for (Int32 i = 0; i < specialtys.Length; i++)
            {
                if (specialtys[i] != -2) answer += Record.GetSpecialtyString(specialtys[i]) + " " + (Single)potientsAtDay[i, 0] / potientsAtDay[i, 1] + "\n";
            }
            return answer;
        }
        /// <summary>
        /// Override ToString.
        /// </summary>
        /// <returns>String object</returns>
        public override String ToString()
        {
            String answer = "";
            for (int i = 0; i < amountRecords; i++)
            {
                answer += records[i].ToString() + "\n";
            }
            return answer;
        }
        /// <summary>
        /// Override Equals.
        /// </summary>
        /// <param name="obj">Check object.</param>
        /// <returns>True if are equal.</returns>
        public override Boolean Equals(object obj)
        {
            if (obj == null || obj.GetType() != this.GetType()) return false;
            MedicalRecords medicalRecordsObj = (MedicalRecords)obj;
            if (this.amountRecords != medicalRecordsObj.amountRecords) return false;
            for (int i = 0; i < medicalRecordsObj.amountRecords; i++)
            {
                if (!Record.Equals(this.records[i], medicalRecordsObj.records[i])) return false;
            }
            return true;
        }
        /// <summary>
        /// Override GetHashCode.
        /// </summary>
        /// <returns>HashCode</returns>
        public override Int32 GetHashCode()
        {
            return records.GetHashCode() ^ amountRecords.GetHashCode();
        }*/
    }
}
