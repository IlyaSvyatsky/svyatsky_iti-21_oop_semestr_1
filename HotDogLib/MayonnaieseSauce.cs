﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotDogLib
{
    class MayonnaiseSauce : HotDog
    {
        public MayonnaiseSauce(string typeOfSausage, string typeOfBun, double cost)
        : base(typeOfSausage, typeOfBun, " Mayonnaise Sauce", cost) { }
    }
}
