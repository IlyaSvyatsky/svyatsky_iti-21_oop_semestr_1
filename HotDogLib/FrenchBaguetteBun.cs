﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotDogLib
{
    class FrenchBaguetteBun : HotDog
    {
        public FrenchBaguetteBun(string typeOfSausage, string typeOfSauce, double cost)
        : base(typeOfSausage, "French Baguette Bun", typeOfSauce, cost) { }
    }
}
