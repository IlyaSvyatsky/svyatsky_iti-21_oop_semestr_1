﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotDogLib
{
    public class HotDog
    {
        private string typeOfSausage;
        private string typeOfBun;
        private string typeOfSauce;
        private double cost;

        private HotDog hotDog;

        public HotDog(string typeOfSausage, string typeOfBun, string typeOfSauce, double cost)
        {
            this.typeOfSausage = typeOfSausage;
            this.typeOfBun = typeOfBun;
            this.typeOfSauce = typeOfSauce;
            this.cost = cost;
        }

        public HotDog()
        {
            hotDog = new HotDog(typeOfSausage, typeOfBun, typeOfSauce, cost);
        }

        public string TypeOfSausage
        {
            get { return typeOfSausage; }
            set { typeOfSausage = value; }
        }

        public string TypeOfBun
        {
            get { return typeOfBun; }
            set { typeOfBun = value; }
        }

        public string TypeOfSauce
        {
            get { return typeOfSauce; }
            set { typeOfSauce = value; }
        }

        public double Cost
        {
            get { return cost; }
            set { cost = value; }
        }
    }
}
