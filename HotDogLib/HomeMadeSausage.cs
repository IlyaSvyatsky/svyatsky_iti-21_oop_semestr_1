﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotDogLib
{
    class HomeMadeSausage : HotDog
    {
        public HomeMadeSausage(string typeOfBun, string typeOfSauce, double cost)
        : base(" HomeMade Sausage", typeOfBun, typeOfSauce, cost) { }
    }
}
