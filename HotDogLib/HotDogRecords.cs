﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotDogLib
{
    public class HotDogRecords
    {
        public HotDog[] arrayOfHotDogs = Array.Empty<HotDog>();

        public int Index { get; set; } = 0;
        public int Length
        { get { return arrayOfHotDogs.Length; } }

        public void AddHotDog(string typeOfSausage, string typeOfBun, string typeOfSauce, double cost)
        {
            Index = arrayOfHotDogs.Length + 1;
            Array.Resize(ref arrayOfHotDogs, Index--);

            if (typeOfSausage == "American Sausage")
            { arrayOfHotDogs[Index] = new AmericanSausage(typeOfBun, typeOfSauce, cost); }
            else if (typeOfSausage == "Bavarian Sausage")
            { arrayOfHotDogs[Index] = new BavarianSausage(typeOfBun, typeOfSauce, cost); }
            else if (typeOfSausage == "HomeMade Sausage")
            { arrayOfHotDogs[Index] = new HomeMadeSausage(typeOfBun, typeOfSauce, cost); }

            if (typeOfBun == "Classical Bun")
            { arrayOfHotDogs[Index] = new ClassicalBun(typeOfSausage, typeOfSauce, cost); }
            else if (typeOfBun == "French Baguette Bun")
            { arrayOfHotDogs[Index] = new FrenchBaguetteBun(typeOfSausage, typeOfSauce, cost); }
            else if (typeOfBun == "Russian HomeMade Bun")
            { arrayOfHotDogs[Index] = new RussianHomeMadeBun(typeOfSausage, typeOfSauce, cost); }

            if (typeOfSauce == "Ketchup Sauce")
            { arrayOfHotDogs[Index] = new KetchupSauce(typeOfSausage, typeOfBun, cost); }
            else if (typeOfSauce == "Mayonnaise Sauce")
            { arrayOfHotDogs[Index] = new MayonnaiseSauce(typeOfSausage, typeOfBun, cost); }
            else if (typeOfSauce == "Mustard Sauce")
            { arrayOfHotDogs[Index] = new MustardSauce(typeOfSausage, typeOfBun, cost); }
        }

        /// <summary>
        /// Общая сумма всех заказов.
        /// </summary>
        /// <returns>Общая сумма всех заказов.</returns>
        public int SumOfAllOrders()
        {
            return arrayOfHotDogs.Length;
        }

        /// <summary>
        /// Количество полных заказов.
        /// </summary>
        /// <returns></returns>
        public double CountOfFullOrders(/*string typeOfSausage, string typeOfBun, string typeOfSauce*/)
        {
            double countOfFullOrders = 0;
            for (int i = 0; i < arrayOfHotDogs.Length; i++)
            {
                if((arrayOfHotDogs[i].TypeOfSausage != "") && (arrayOfHotDogs[i].TypeOfBun != "") && (arrayOfHotDogs[i].TypeOfSauce != ""))
                {
                    countOfFullOrders++;
                }
            }
            return countOfFullOrders;
        }

        /// </summary>
        /// Средняя цена 
        /// </summary>
        /// <returns></returns>
        public double AverageOrderValueWithABunOnly()
        {
            double averageCost = 0;
            float count = 0;
            for (int i = 0; i < arrayOfHotDogs.Length; i++)
            {
                if ((arrayOfHotDogs[i].TypeOfSausage == "" || arrayOfHotDogs[i].TypeOfSausage == null) && (arrayOfHotDogs[i].TypeOfSauce == "" || arrayOfHotDogs[i].TypeOfSauce == null))
                {
                    count++;
                    averageCost += arrayOfHotDogs[i].Cost;
                }
            }
            if (count == 0)
            {
                averageCost = 0;
            }
            else
            {
                averageCost = averageCost / count;
            }
            return averageCost;
        }
    }
}
