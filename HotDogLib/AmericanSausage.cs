﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotDogLib 
{
    class AmericanSausage : HotDog
    {
        public AmericanSausage(string typeOfBun, string typeOfSauce, double cost)
        : base("American Sausage", typeOfBun, typeOfSauce, cost) { }
    }
}
