﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotDogLib
{
    class MustardSauce : HotDog
    {
        public MustardSauce(string typeOfSausage, string typeOfBun, double cost)
        : base(typeOfSausage, typeOfBun, "Mustard Sauce", cost) { }
    }
}
