﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotDogLib
{
    class KetchupSauce : HotDog
    {
        public KetchupSauce(string typeOfSausage, string typeOfBun, double cost)
        : base(typeOfSausage, typeOfBun, "Ketchup Sauce", cost) { }
    }
}
