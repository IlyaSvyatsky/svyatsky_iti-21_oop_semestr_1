﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotDogLib
{
    class RussianHomeMadeBun : HotDog
    {
        public RussianHomeMadeBun(string typeOfSausage, string typeOfSauce, double cost)
        : base(typeOfSausage, "Russian HomeMade Bun", typeOfSauce, cost) { }
    }
}
