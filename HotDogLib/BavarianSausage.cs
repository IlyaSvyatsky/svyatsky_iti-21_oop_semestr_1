﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotDogLib
{
    class BavarianSausage : HotDog
    {
        public BavarianSausage(string typeOfBun, string typeOfSauce, double cost)
        : base("Bavarian Sausage", typeOfBun, typeOfSauce, cost) { }
    }
}
