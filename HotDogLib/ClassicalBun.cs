﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotDogLib
{
    class ClassicalBun : HotDog
    {
        public ClassicalBun(string typeOfSausage, string typeOfSauce, double cost)
        : base(typeOfSausage, "Classical Bun", typeOfSauce, cost) { }
    }
}
