﻿namespace OOPCSharp6WF
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.productUsageDate = new System.Windows.Forms.DateTimePicker();
            this.typeOfProduct = new System.Windows.Forms.ComboBox();
            this.textBoxCalories = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.textBoxNameOfProduct = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.CaloryByProductType = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.AverageCalories = new System.Windows.Forms.Button();
            this.richTextBoxRecord = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.Control;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Location = new System.Drawing.Point(9, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Выберите нужную дату:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.Control;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(141, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(162, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Тип употреблённого продукта:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.Control;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(502, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(163, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "Введите кол-во калорий(ккал):";
            // 
            // productUsageDate
            // 
            this.productUsageDate.Location = new System.Drawing.Point(12, 25);
            this.productUsageDate.Name = "productUsageDate";
            this.productUsageDate.Size = new System.Drawing.Size(126, 20);
            this.productUsageDate.TabIndex = 24;
            // 
            // typeOfProduct
            // 
            this.typeOfProduct.FormattingEnabled = true;
            this.typeOfProduct.Items.AddRange(new object[] {
            "Фрукты",
            "Злаки",
            "Овощи",
            "Травы и пряности",
            "Соки",
            "Мясо",
            "Молочное",
            "Яйца",
            "Моллюски и ракообразные",
            "Насекомые",
            "Грибы",
            "Прочее органическое"});
            this.typeOfProduct.Location = new System.Drawing.Point(144, 25);
            this.typeOfProduct.Name = "typeOfProduct";
            this.typeOfProduct.Size = new System.Drawing.Size(159, 21);
            this.typeOfProduct.TabIndex = 25;
            this.typeOfProduct.Text = "Фрукты";
            // 
            // textBoxCalories
            // 
            this.textBoxCalories.Location = new System.Drawing.Point(505, 25);
            this.textBoxCalories.Name = "textBoxCalories";
            this.textBoxCalories.Size = new System.Drawing.Size(159, 20);
            this.textBoxCalories.TabIndex = 26;
            this.textBoxCalories.TextChanged += new System.EventHandler(this.textBoxCalories_TextChanged);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.Control;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button1.Location = new System.Drawing.Point(670, 10);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(118, 36);
            this.button1.TabIndex = 27;
            this.button1.Text = "Добавить продукт";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 60);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(291, 37);
            this.button2.TabIndex = 30;
            this.button2.Text = "Вывести список употреблённых продуктов за указанный день";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // textBoxNameOfProduct
            // 
            this.textBoxNameOfProduct.Location = new System.Drawing.Point(309, 25);
            this.textBoxNameOfProduct.Name = "textBoxNameOfProduct";
            this.textBoxNameOfProduct.Size = new System.Drawing.Size(190, 20);
            this.textBoxNameOfProduct.TabIndex = 31;
            this.textBoxNameOfProduct.TextChanged += new System.EventHandler(this.textBoxNameOfProduct_TextChanged_1);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.Control;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Location = new System.Drawing.Point(306, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(193, 13);
            this.label4.TabIndex = 32;
            this.label4.Text = "Название употреблённого продукта:";
            // 
            // CaloryByProductType
            // 
            this.CaloryByProductType.BackColor = System.Drawing.SystemColors.Control;
            this.CaloryByProductType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CaloryByProductType.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CaloryByProductType.Location = new System.Drawing.Point(12, 113);
            this.CaloryByProductType.Name = "CaloryByProductType";
            this.CaloryByProductType.Size = new System.Drawing.Size(291, 36);
            this.CaloryByProductType.TabIndex = 33;
            this.CaloryByProductType.Text = "Вывести по заданному типу продукта кол-во употреблённых калорий";
            this.CaloryByProductType.UseVisualStyleBackColor = false;
            this.CaloryByProductType.Click += new System.EventHandler(this.CaloryByProductType_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.Control;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label5.Location = new System.Drawing.Point(177, 166);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(23, 13);
            this.label5.TabIndex = 34;
            this.label5.Text = "От:";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "dd.MM.yyyy hh:mm";
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(180, 182);
            this.dateTimePicker1.MaxDate = new System.DateTime(2020, 1, 31, 0, 0, 0, 0);
            this.dateTimePicker1.MinDate = new System.DateTime(2002, 1, 1, 0, 0, 0, 0);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(123, 20);
            this.dateTimePicker1.TabIndex = 35;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.SystemColors.Control;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label6.Location = new System.Drawing.Point(177, 221);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(25, 13);
            this.label6.TabIndex = 36;
            this.label6.Text = "До:";
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.CustomFormat = "dd.MM.yyyy hh:mm";
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker2.Location = new System.Drawing.Point(180, 237);
            this.dateTimePicker2.MaxDate = new System.DateTime(2020, 12, 31, 0, 0, 0, 0);
            this.dateTimePicker2.MinDate = new System.DateTime(2002, 1, 1, 0, 0, 0, 0);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(123, 20);
            this.dateTimePicker2.TabIndex = 37;
            this.dateTimePicker2.ValueChanged += new System.EventHandler(this.dateTimePicker2_ValueChanged);
            // 
            // AverageCalories
            // 
            this.AverageCalories.BackColor = System.Drawing.SystemColors.Control;
            this.AverageCalories.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AverageCalories.ForeColor = System.Drawing.SystemColors.ControlText;
            this.AverageCalories.Location = new System.Drawing.Point(12, 166);
            this.AverageCalories.Name = "AverageCalories";
            this.AverageCalories.Size = new System.Drawing.Size(159, 91);
            this.AverageCalories.TabIndex = 38;
            this.AverageCalories.Text = "За указанный период вывести среднее кол-во употреблённых калорий";
            this.AverageCalories.UseVisualStyleBackColor = false;
            this.AverageCalories.Click += new System.EventHandler(this.AverageCalories_Click);
            // 
            // richTextBoxRecord
            // 
            this.richTextBoxRecord.Location = new System.Drawing.Point(309, 60);
            this.richTextBoxRecord.Name = "richTextBoxRecord";
            this.richTextBoxRecord.ReadOnly = true;
            this.richTextBoxRecord.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.richTextBoxRecord.Size = new System.Drawing.Size(479, 358);
            this.richTextBoxRecord.TabIndex = 28;
            this.richTextBoxRecord.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 430);
            this.Controls.Add(this.AverageCalories);
            this.Controls.Add(this.dateTimePicker2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.CaloryByProductType);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxNameOfProduct);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.richTextBoxRecord);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBoxCalories);
            this.Controls.Add(this.typeOfProduct);
            this.Controls.Add(this.productUsageDate);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Name = "Form1";
            this.Text = "Lab6";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker productUsageDate;
        private System.Windows.Forms.ComboBox typeOfProduct;
        private System.Windows.Forms.TextBox textBoxCalories;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBoxNameOfProduct;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button CaloryByProductType;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Button AverageCalories;
        private System.Windows.Forms.RichTextBox richTextBoxRecord;
    }
}

