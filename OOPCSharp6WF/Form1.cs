﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CaloryLib;

namespace OOPCSharp6WF
{
    public partial class Form1 : Form
    {
        public Calories calories;
        public Form1()
        {
            InitializeComponent();
            this.calories = new Calories();
        }

        private void textBoxCalories_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxNameOfProduct_TextChanged(object sender, EventArgs e)
        {

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            int productType = typeOfProduct.SelectedIndex;
            DateTime recordUsageDate = productUsageDate.Value;
            double productCalories = Convert.ToDouble(textBoxCalories.Text);
            calories.AddProductRecord(recordUsageDate, productType, productCalories, textBoxNameOfProduct.Text != "" ? textBoxNameOfProduct.Text : "name of product");
            textBoxNameOfProduct.Text = "";
            richTextBoxRecord.Text = calories.ToString();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show(calories.ListOfUsedProductsByDate(productUsageDate.Value));
        }

        private void CaloryByProductType_Click(object sender, EventArgs e)
        {
            MessageBox.Show(calories.CaloriesByProductType(productUsageDate.Value, typeOfProduct.SelectedIndex, calories.GetNumberOfCalories));
        }

        private void AverageCalories_Click(object sender, EventArgs e)
        {
            MessageBox.Show(calories.AverageCalories(dateTimePicker1.Value, dateTimePicker2.Value, typeOfProduct.SelectedIndex, calories.GetNumberOfCalories));
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {

        }

        private void textBoxNameOfProduct_TextChanged_1(object sender, EventArgs e)
        {

        }
    }
}
