﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Lab8Library
{
    public class Polygon : GeometricFigure, IComparable<Polygon>
    {
        public double[] ArrayOfCoordinats { get; set; }

        protected Int32 amountSide;
        protected Int32 side;

        public override Double GetSquare()
        {
            return amountSide * side * side / 4 * Math.Tanh(180.0 / amountSide);
        }

        public int CompareTo(Polygon other)
        {
            return (int)(GetSquare() - other.GetSquare());
        }
    }
}
