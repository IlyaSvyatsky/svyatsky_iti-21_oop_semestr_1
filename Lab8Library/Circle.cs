﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Lab8Library
{
    class Circle : Polygon
    {
        private int radius;
        private int posX;
        private int posY;

        public Circle(Color colorOfFigure, int radius, int posX, int PosY)
        {
            ColorOfFigure = colorOfFigure;
            this.radius = radius;
            this.posX = posX;
            this.posY = PosY;
        }

        public override Double GetSquare()
        {
            return Math.PI * radius * radius;
        }

        public Double CalculateLenght()
        {
            return 2 * Math.PI * radius;
        }

        public override String ToString()
        {
            String answer = "";
            answer += "Radius: " + radius + " ";
            answer += "posX: " + posX + " ";
            answer += "posY: " + posY + "\n";
            answer += "Square: " + GetSquare() + "\n";
            return answer;
        }

        public Boolean IsFirstQuarter()
        {
            if ((posX - radius > 0) && (posY - radius > 0)) return true;
            else return false;
        }
    }



    /*public class Circle : Polygon
    {
        private double x;
        private double y;
        private double r;

        public double S
        {
            get
            {
                return Math.PI * r * r;
            }
        }

        public double this[string nameOfParam]
        {
            get
            {
                switch (nameOfParam)
                {
                    case "x":
                        return x;
                    case "y":
                        return y;
                    case "r":
                        return r;
                }
                return 0;
            }
            set
            {
                switch (nameOfParam)
                {
                    case "x":
                        x = value;
                        break;
                    case "y":
                        y = value;
                        break;
                    case "r":
                        r = value;
                        break;
                }
            }
        }

        public Circle(double x, double y, double r)
        {
            this.x = x;
            this.y = y;
            this.r = r;
        }

       
    }*/
}
