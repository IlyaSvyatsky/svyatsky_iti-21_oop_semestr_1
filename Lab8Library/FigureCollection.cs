﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Lab8Library
{
    public class FigureCollection
    {
        public Polygon[] arrayOfFigures;

        public FigureCollection()
        {
            arrayOfFigures = Array.Empty<Polygon>();
        }

        public void AddFigures()
        {
            using (StreamReader sr = new StreamReader("input.txt", System.Text.Encoding.Default))
            {
                arrayOfFigures = Array.Empty<Polygon>();

                Color colorOfFigure;
                double[] arrayOfCoordinates;

                int fileIndex = 0;

                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    string[] strArray = line.Split(' ');
                    
                    arrayOfCoordinates = new double[strArray.Length - 1];
                    int i = 0;
                    for (i=0; i < strArray.Length - 1; i++)
                    {
                        arrayOfCoordinates[i] = int.Parse(strArray[i]);
                    }
                    colorOfFigure = Color.FromName(strArray[strArray.Length - 1]);
                    Array.Resize(ref arrayOfFigures, arrayOfFigures.Length + 1);
                    if (strArray.Length - 1 == 6)
                        arrayOfFigures[fileIndex] = new Triangle(colorOfFigure, arrayOfCoordinates);
                    if (strArray.Length - 1 == 8)
                        arrayOfFigures[fileIndex] = new Rectangle(colorOfFigure, arrayOfCoordinates);
                    fileIndex++;
                }
            }
        }

        public void DeleteFigure(int index)
        {
            Polygon[] tempArray = new Polygon[arrayOfFigures.Length - 1];
            Array.Copy(arrayOfFigures, 0, tempArray, 0, index);
            Array.Copy(arrayOfFigures, index + 1, tempArray, index, arrayOfFigures.Length - index - 1);
            arrayOfFigures = tempArray;
        }

        public string GetQuater(int index)
        {
            string str = "";
            if (arrayOfFigures[index].ArrayOfCoordinates.Length == 6 || arrayOfFigures[index].ArrayOfCoordinates.Length == 8)
            {
                int quarter1, quarter2, quarter3, quarter4, quarterError;
                quarter1 = quarter2 = quarter3 = quarter4 = quarterError = 0;

                for (int i = 0; i < arrayOfFigures[index].ArrayOfCoordinates.Length; i += 2)
                {
                    if (arrayOfFigures[index].ArrayOfCoordinates[i] > 0 && arrayOfFigures[index].ArrayOfCoordinates[i + 1] > 0)
                        quarter1 += 2;
                    if (arrayOfFigures[index].ArrayOfCoordinates[i] < 0 && arrayOfFigures[index].ArrayOfCoordinates[i + 1] > 0)
                        quarter2 += 2;
                    if (arrayOfFigures[index].ArrayOfCoordinates[i] < 0 && arrayOfFigures[index].ArrayOfCoordinates[i + 1] < 0)
                        quarter3 += 2;
                    if (arrayOfFigures[index].ArrayOfCoordinates[i] > 0 && arrayOfFigures[index].ArrayOfCoordinates[i + 1] < 0)
                        quarter4 += 2;
                }
                if (quarter1 == arrayOfFigures[index].ArrayOfCoordinates.Length)
                    str = "Первая четверть;";
                else
                    quarterError++;
                if (quarter2 == arrayOfFigures[index].ArrayOfCoordinates.Length)
                    str = "Вторая четверть;";
                else
                    quarterError++;
                if (quarter3 == arrayOfFigures[index].ArrayOfCoordinates.Length)
                    str = "Третья четверть;";
                else
                    quarterError++;
                if (quarter4 == arrayOfFigures[index].ArrayOfCoordinates.Length)
                    str = "Четвертая четверть;";
                else
                    quarterError++;

                if (quarterError == 4)
                    str = "Часть фигуры лежит на нескольких четвертях или на прямой X/Y;";
            }
            return str;
        }
        public int GetQuaterInt(int index)
        {
            int result = 0;
            if (arrayOfFigures[index].ArrayOfCoordinates.Length == 6 || arrayOfFigures[index].ArrayOfCoordinates.Length == 8)
            {
                int quarter1, quarter2, quarter3, quarter4, quarterError;
                quarter1 = quarter2 = quarter3 = quarter4 = quarterError = 0;

                for (int i = 0; i < arrayOfFigures[index].ArrayOfCoordinates.Length; i += 2)
                {
                    if (arrayOfFigures[index].ArrayOfCoordinates[i] > 0 && arrayOfFigures[index].ArrayOfCoordinates[i + 1] > 0)
                        quarter1 += 2;
                    if (arrayOfFigures[index].ArrayOfCoordinates[i] < 0 && arrayOfFigures[index].ArrayOfCoordinates[i + 1] > 0)
                        quarter2 += 2;
                    if (arrayOfFigures[index].ArrayOfCoordinates[i] < 0 && arrayOfFigures[index].ArrayOfCoordinates[i + 1] < 0)
                        quarter3 += 2;
                    if (arrayOfFigures[index].ArrayOfCoordinates[i] > 0 && arrayOfFigures[index].ArrayOfCoordinates[i + 1] < 0)
                        quarter4 += 2;
                }
                if (quarter1 == arrayOfFigures[index].ArrayOfCoordinates.Length)
                    result = 1;
                else
                    quarterError++;
                if (quarter2 == arrayOfFigures[index].ArrayOfCoordinates.Length)
                    result = 2;
                else
                    quarterError++;
                if (quarter3 == arrayOfFigures[index].ArrayOfCoordinates.Length)
                    result = 3;
                else
                    quarterError++;
                if (quarter4 == arrayOfFigures[index].ArrayOfCoordinates.Length)
                    result = 4;
                else
                    quarterError++;

                if (quarterError == 4)
                    result = 0;
            }
            return result;
        }
        public bool IsRectangle(int index)
        {
            bool result = false;
            if(arrayOfFigures[index].ArrayOfCoordinates.Length == 6)
            {
                double[] tempArr = new double[3];
                tempArr[0] = Math.Sqrt(Math.Pow((arrayOfFigures[index].ArrayOfCoordinates[0] - arrayOfFigures[index].ArrayOfCoordinates[2]), 2) + Math.Pow(arrayOfFigures[index].ArrayOfCoordinates[1] - arrayOfFigures[index].ArrayOfCoordinates[3], 2));
                tempArr[1] = Math.Sqrt(Math.Pow((arrayOfFigures[index].ArrayOfCoordinates[0] - arrayOfFigures[index].ArrayOfCoordinates[4]), 2) + Math.Pow(arrayOfFigures[index].ArrayOfCoordinates[1] - arrayOfFigures[index].ArrayOfCoordinates[5], 2));
                tempArr[2] = Math.Sqrt(Math.Pow((arrayOfFigures[index].ArrayOfCoordinates[2] - arrayOfFigures[index].ArrayOfCoordinates[4]), 2) + Math.Pow(arrayOfFigures[index].ArrayOfCoordinates[3] - arrayOfFigures[index].ArrayOfCoordinates[5], 2));
                Array.Sort(tempArr);
                if ( Math.Pow(tempArr[2], 2) == ( Math.Pow(tempArr[1], 2) + Math.Pow(tempArr[0], 2) ))
                { result = true; }
                else
                { result = false; }
            }
            else
            {
                result = false;
            }
            
            return result;
        }
    }
}
