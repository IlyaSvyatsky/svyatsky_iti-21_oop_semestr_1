﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Lab8Library
{
    public class Triangle : Polygon
    {
        public Triangle(Color colorOfFigure, params double[] parametrs)
        {
            ColorOfFigure = colorOfFigure;
            ArrayOfCoordinates = parametrs;
        }

        public override double GetSquare()
        {
            // S = 1/2|(x1-x3)*(y2-y3) - (x2-x3)*(y1-y3)|
            return 0.5 * Math.Abs((ArrayOfCoordinates[0] - ArrayOfCoordinates[4]) * (ArrayOfCoordinates[3] - ArrayOfCoordinates[5]) -
                (ArrayOfCoordinates[2] - ArrayOfCoordinates[4]) * (ArrayOfCoordinates[1] - ArrayOfCoordinates[5]));
        }
    }
}
