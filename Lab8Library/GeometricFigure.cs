﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Lab8Library
{
    public abstract class GeometricFigure
    {
        public double[] ArrayOfCoordinates { get; set; }
        public Color ColorOfFigure { get; set; }
        public abstract double GetSquare();

        public string Display()
        {
            string str = "Тип фигуры: " + base.ToString();
            str += "; Цвет фигуры: " + ColorOfFigure.ToString() + ";";
            str += "; Координаты: ";

            for (int i = 0; i < ArrayOfCoordinates.Length; i += 2)
            {
                str += "(" + ArrayOfCoordinates[i].ToString() + ", " + ArrayOfCoordinates[i + 1].ToString() + ") ";
            }
            str += "; Площадь фигуры: " + GetSquare().ToString();
            return str;
        }
    }

    /*public abstract class GeometricFigure
    {
        public double[] ArrayOfCoordinates { get; set; }
        public Color ColorOfFigure { get; set; }
        public abstract double GetSquare();

        public string Display()
        {
            string str = "Тип фигуры: " + base.ToString();
            str += "; Координаты: ";

            for (int i = 0; i < ArrayOfCoordinates.Length; i += 2)
            {
                str += "(" + ArrayOfCoordinates[i].ToString() + ", " + ArrayOfCoordinates[i + 1].ToString() + ") ";
            }

            str += "; Площадь: " + GetSquare().ToString();
            str += "; Цвет фигуры: " + ColorOfFigure.ToString() + ";";
            return str;
        }
    }*/

    public class FigureComparer : IComparer<GeometricFigure>
    {
        public int Compare(GeometricFigure a, GeometricFigure b)
        {
            if (a.GetSquare() > b.GetSquare())
                return 1;
            if (a.GetSquare() < b.GetSquare())
                return -1;
            else
                return 0;
        }
    }
}
