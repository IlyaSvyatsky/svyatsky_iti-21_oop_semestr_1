﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Lab8Library
{
    public class Rectangle : Polygon
    {
        public Rectangle(Color colorOfFigure, params double[] parametrs)
        {
            ColorOfFigure = colorOfFigure;
            ArrayOfCoordinates = parametrs;
        }

        public override double GetSquare()
        {
            double sideA = Math.Sqrt((ArrayOfCoordinates[0] - ArrayOfCoordinates[2]) * (ArrayOfCoordinates[0] - ArrayOfCoordinates[2]) +
                (ArrayOfCoordinates[1] - ArrayOfCoordinates[3]) * (ArrayOfCoordinates[1] - ArrayOfCoordinates[3]));
            double sideB = Math.Sqrt((ArrayOfCoordinates[4] - ArrayOfCoordinates[2]) * (ArrayOfCoordinates[4] - ArrayOfCoordinates[2]) +
                (ArrayOfCoordinates[5] - ArrayOfCoordinates[3]) * (ArrayOfCoordinates[5] - ArrayOfCoordinates[3]));
            return sideA * sideB;
        }
    }
}
