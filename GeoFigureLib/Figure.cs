﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace GeoFigureLib
{
    public class Figure
    {
        public double[] ArrayOfCoordinates { get; set; }
        public Color ColorOfFigure { get; set; }
        public abstract double GetSquare();

        public string Display()
        {
            string str = "Тип фигуры: " + base.ToString();
            str += "; Координаты: ";

            for (int i = 0; i < ArrayOfCoordinates.Length; i += 2)
            {
                str += "(" + ArrayOfCoordinates[i].ToString() + ", " + ArrayOfCoordinates[i + 1].ToString() + ") ";
            }

            str += "; Площадь: " + GetSquare().ToString();
            str += "; Цвет фигуры: " + ColorOfFigure.ToString() + ";";
            return str;
        }
    }
}
