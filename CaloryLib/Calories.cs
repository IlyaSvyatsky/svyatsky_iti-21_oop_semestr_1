﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaloryLib
{

    enum TypeOfProduct
    {
        Fruit = 0,
        Cereals = 1,
        Vegetables = 2,
        HerbsAndSpices = 3, //травы и пряности
        Juices = 4,
        Meat = 5,
        Dairy = 6, //молочное
        Eggs = 7,
        ShellfishAndCrustaceans = 8, //Моллюски и ракообразные 
        Insects = 9, //Насекомые
        Mushrooms = 10,
        OtherOrganic = 11 //Прочее органическое
    }

    public class Calories
    {

        private class ProductRecord
        {
            /// <summary>
            /// Тип продукта.
            /// </summary>
            public string TypeOfProduct { get; private set; }

            /// <summary>
            /// Название продукта
            /// </summary>
            public string NameOfProduct { get; private set; }

            /// <summary>
            /// Количество калорий.
            /// </summary>
            public double NumberOfCalories { get; private set; }

            /// <summary>
            /// Количество записей.
            /// </summary>
            public int NumberOfRecords { get; private set; }

            /// <summary>
            /// Дата использования продукта.
            /// </summary>
            public DateTime ProductUsageDate { get; private set; }

            /// <summary>
            /// Дата конечного периода.
            /// </summary>
            public DateTime EndDate { get; private set; }

            /// <summary>
            /// Конструктор записей.
            /// </summary>
            /// <param name="productUsageDate">Дата использования.</param>
            /// <param name="numberOfRecords">Кол-во записей.</param>
            /// <param name="numberOfCalories">Кол-во калорий.</param>
            /// <param name="nameOfProduct">Прудукт.</param>
            public ProductRecord(int numberOfRecords, string nameOfProduct, double numberOfCalories, DateTime productUsageDate)
            {
                this.NumberOfRecords = numberOfRecords;
                this.TypeOfProduct = Enum.GetName(typeof(TypeOfProduct), numberOfRecords);
                this.NameOfProduct = nameOfProduct;
                this.NumberOfCalories = numberOfCalories;
                this.ProductUsageDate = productUsageDate;
                this.EndDate = productUsageDate;
            }

            /// <summary>
            /// 1
            /// </summary>
            /// <returns></returns>
            public override string ToString()
            {
                string result = "";
                result += " Тип продукта: " + TypeOfProduct + "\n" + " Название продукта: " + NameOfProduct + "\n" + " Кол-во калорий: " + NumberOfCalories + " ккал " + "\n" + " Дата употребления: " + ProductUsageDate.ToShortDateString() + "\n" + "\n";
                return result;
            }
        }

        public double GetNumberOfCalories { get; private set; }

        /// <summary>
        /// Массив пустой.
        /// </summary>
        private ProductRecord[] arrayOfProductRecords = Array.Empty<ProductRecord>();

        public int Index { get; set; } = 0;

        /// <summary>
        /// Добавление записей в одноименный массив.
        /// </summary>
        /// <param name="productUsageDate">Дата.</param>
        /// <param name="numberOfRecords">Кол-во записей.</param>
        /// <param name="numberOfCalories">Кол-во калорий.</param>
        /// <param name="nameOfProduct">Продукт.</param>
        public void AddProductRecord(DateTime productUsageDate, int numberOfRecords, double numberOfCalories, string nameOfProduct)
        {
            Index = arrayOfProductRecords.Length + 1;
            Array.Resize(ref arrayOfProductRecords, Index--);
            arrayOfProductRecords[Index] = new ProductRecord(numberOfRecords, nameOfProduct, numberOfCalories, productUsageDate);
        }

        /// <summary>
        /// fyu
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string answer = "";
            for (int i = 0; i <= Index; i++)
            {
                answer += arrayOfProductRecords[i].ToString() + "\n";
            }
            return answer;
        }

        /// <summary>
        /// Возвращает массив записей о продуктах за указанный день.
        /// </summary>
        /// <param name="productUsageDate">Проверка даты.</param>
        /// <returns>Массив имен продуктов.</returns>
        private string[] GetProducts(DateTime productUsageDate)
        {
            string[] products = new string[Index];
            int amountFood = 0;
            for (int i = 0; i <= Index; i++)
            {
                if (arrayOfProductRecords[i].ProductUsageDate.Date == productUsageDate.Date)
                {
                    products[amountFood] = arrayOfProductRecords[i].NameOfProduct;
                    amountFood++;
                }
            }
            return (string[])products.Clone();
        }

        /// <summary>
        /// Возвращает имена продуктов за указанный день.
        /// </summary>
        /// <param name=" productUsageDate">Дата.</param>
        /// <returns>Строка с именованием продуктов.</returns>
        public string ListOfUsedProductsByDate(DateTime productUsageDate)
        {
            string[] products = GetProducts(productUsageDate);
            string result = "";
            for (int i = 0; i < products.Length; i++)
            {
                result += products[i] + "\n";
            }
            return result;
        }

        /// <summary>
        /// Количество потреблённых калорий в день по типу продукта.
        /// </summary>
        /// <param name="productUsageDate"></param>
        /// <param name="numberOfRecords"></param>
        /// <param name="numberOfCalories"></param>
        /// <returns>Строка со значением потреблённых калорий в день.</returns>
        public string CaloriesByProductType(DateTime productUsageDate, int numberOfRecords, double numberOfCalories)
        {
            double countCalories = 0;
            for (int i = 0; i < arrayOfProductRecords.Length; i++)
            {
                if (arrayOfProductRecords[i].ProductUsageDate.Date == productUsageDate.Date)
                {
                    if (arrayOfProductRecords[i].NumberOfRecords == numberOfRecords)
                    {
                        countCalories = countCalories + arrayOfProductRecords[i].NumberOfCalories;
                    }
                }
            }
            string result = " Количество употреблённых калорий по определенному типу продукта за "  + productUsageDate.ToShortDateString() + " составляет: " + Convert.ToString(countCalories);
            return result;
        }

        /// <summary>
        /// Среднее количество аотребленных калорий за указаный период.
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="finishDate"></param>
        /// <param name="numberOfOrder"></param>
        /// <param name="numberOfCalories"></param>
        /// <returns>Строка со средним количеством потреблённых калорий</returns>
        public string AverageCalories(DateTime startDate, DateTime finishDate, int numberOfOrder, double numberOfCalories)
        {
            DateTime lowEndDate, highEndDate;
            if (startDate <= finishDate)
            {
                lowEndDate = startDate;
                highEndDate = finishDate;
            }
            else
            {
                lowEndDate = finishDate;
                highEndDate = startDate;
            }
            string result = "";
            int i;
            double countCalories = 0;
            int amountCalories = 0;
            for (i = 0; i < arrayOfProductRecords.Length; i++)
            {

                if (arrayOfProductRecords[i].ProductUsageDate >= lowEndDate && arrayOfProductRecords[i].EndDate <= highEndDate)
                {
                    countCalories = countCalories + arrayOfProductRecords[i].NumberOfCalories;
                    amountCalories++;
                }
            }
            double averageCalories = countCalories/ amountCalories;
            result = " Среднее количество употреблённых калорий с " + lowEndDate.ToShortDateString() + " по " + highEndDate.ToShortDateString() + " составляет: " + Convert.ToString(averageCalories);
            return result;
        }
    }
}


/*using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaloryLib
{
    enum TypeOfProduct
    {
        Fruit = 0,
        Cereals = 1,
        Vegetables = 2,
        HerbsAndSpices = 3, //травы и пряности
        Juices = 4,
        Meat = 5,
        Dairy = 6, //молочное
        Eggs = 7,
        ShellfishAndCrustaceans = 8, //Моллюски и ракообразные 
        Insects = 9, //Насекомые
        Mushrooms = 10,
        OtherOrganic = 11 //Прочее органическое
    }

    public class Calories
    {
        private class ProductRecord
        {
            
            public DateTime productUsageDate;
            DateTime endDate;

            string typeOfProduct;
            string nameOfProduct;
            double numberOfCalories;
            int numberOfRecords;

            public ProductRecord(DateTime productUsageDate, int numberOfRecords, double numberOfCalories, string nameOfProduct)
            {
                this.productUsageDate = productUsageDate;
                this.endDate = productUsageDate;
                this.typeOfProduct = Enum.GetName(typeof(TypeOfProduct), numberOfRecords);
                this.numberOfCalories = numberOfCalories;
                this.numberOfRecords = numberOfRecords;
                this.nameOfProduct = nameOfProduct;
            }

            internal DateTime ProductUsageDate
            {
                get { return productUsageDate; }
                set { productUsageDate = value; }
            }
            internal DateTime EndDate
            {
                get { return endDate; }
                set { endDate = value; }
            }
            internal string TypeOfProduct
            {
                get { return typeOfProduct; }
                set { typeOfProduct = value; }
            }
            internal string NameOfProduct
            {
                get { return nameOfProduct; }
                set { nameOfProduct = value; }
            }
            internal double NumberOfCalories
            {
                get { return numberOfCalories; }
                set { numberOfCalories = value; }
            }

            internal int NumberOfRecords
            {
                get { return numberOfRecords; }
                set { numberOfRecords = value; }
            }

            public override String ToString()
            {
                String result = "";
                result += " Тип продукта: " + typeOfProduct + "\n" + " Название продукта: " + nameOfProduct + "\n" + " Кол-во калорий: " + numberOfCalories + " ккал " + "\n" + " Дата употребления: " + productUsageDate.ToShortDateString() + "\n" + "\n";
                return result;
            }
        }

        /// <summary>
        /// Свойство для доступа к жанру книги;
        /// </summary>    
        public double GetNumberOfCalories
        {
            get { return arrayOfProductRecords[Index].NumberOfCalories; }
            set { arrayOfProductRecords[Index].NumberOfCalories = value; }
        }


        private ProductRecord[] arrayOfProductRecords = Array.Empty<ProductRecord>();
        public int Index { get; set; } = 0;

        public void AddProductRecord(DateTime productUsageDate, int numberOfRecords, double numberOfCalories, string nameOfProduct)
        {
            Index = arrayOfProductRecords.Length + 1;
            Array.Resize(ref arrayOfProductRecords, Index--);
            arrayOfProductRecords[Index] = new ProductRecord(productUsageDate, numberOfRecords, numberOfCalories, nameOfProduct);
        }

        public override String ToString()
        {
            String answer = "";
            for (int i = 0; i <= Index; i++)
            {
                answer += arrayOfProductRecords[i].ToString() + "\n";
            }
            return answer;
        }
        


        private string[] GetProducts(DateTime productUsageDate)
        {
            string[] products = new string[Index];
            int amountFood = 0;
            for (int i = 0; i < Index; i++)
            {
                if (arrayOfProductRecords[i].productUsageDate.Date == productUsageDate.Date)
                {
                    products[amountFood] = arrayOfProductRecords[i].NameOfProduct;
                    amountFood++;
                }
            }
            return (string[])products.Clone();
        }

        public string ListOfUsedProductsByDate(DateTime date)
        {
            string[] products = GetProducts(date);
            string answer = "";
            for (Int32 i = 0; i < products.Length; i++)
            {
                answer += products[i] + "\n";
            }
            return answer;
        }


        public string CaloriesByProductType(DateTime productUsageDate, int numberOfRecords, double numberOfCalories)
        {
            double countCalories = 0;
            for (int i = 0; i < arrayOfProductRecords.Length; i++)
            {
                if (arrayOfProductRecords[i].productUsageDate.Date == productUsageDate.Date)
                {
                    if (arrayOfProductRecords[i].NumberOfRecords == numberOfRecords)
                    {
                        countCalories = countCalories + arrayOfProductRecords[i].NumberOfCalories;
                    }
                }
            }
            string result = "Количество употреблённых калорий в день: " + Convert.ToString(countCalories);
            return result;
        }

        public string AverageCalories(DateTime startDate, DateTime finishDate, int numberOfOrder, double numberOfCalories)
        {
            DateTime lowEndDate, highEndDate;
            if (startDate <= finishDate)
            {
                lowEndDate = startDate;
                highEndDate = finishDate;
            }
            else
            {
                lowEndDate = finishDate;
                highEndDate = startDate;
            }
            string result = "";
            int i;
            double countCalories = 0;
            for (i = 0; i < arrayOfProductRecords.Length; i++)
            {

                if (arrayOfProductRecords[i].productUsageDate >= lowEndDate && arrayOfProductRecords[i].EndDate <= highEndDate)
                {
                    countCalories = countCalories + arrayOfProductRecords[i].NumberOfCalories;
                }
            }
            double averageCalories = (highEndDate - lowEndDate).TotalDays / countCalories;
            result = " Среднее количество употреблённых калорий в день: " + Convert.ToString(averageCalories);
            return result;
        }
    }
}*/
