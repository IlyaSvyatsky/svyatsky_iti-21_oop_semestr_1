﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

///<summary>
/// Пространство имен содержит все родительские / дочерние классы полинома.
/// </summary>
namespace PolynomialLib
{

    /// <summary>
    /// Дочерний класс, который содержит методы для работы с полиномом.
    /// </summary>
    public class Polynom
    {
        double[] array; // коэффициенты полинома 
        int degree; // степень полиномаFF

        ///<summary>
        ///Конструктор, который инициализирует поля класса полинома с указанием степени полинома.
        /// </summary>
        public Polynom(int degree)
        {
            this.degree = degree;
            array = new double[degree];
        }

        // индексатор для получения коэффициента полинома по степени
        public double this[int i]
        {
            get { return array[i]; }
            set { array[i] = value; }
        }

        /// <summary>
        /// Метод преобразования полинома в строку.
        /// </summary>
        /// <return> Строка, формирующая полином </return>
        public string SPolynom()
        {
                string result = "";
                if (degree == 0)
                {
                    return result;
                }
                if (degree == 1)
                {
                    return result += array[degree - 1] + " = 0";
                }
                result += array[degree - 1] + "*x^" + (degree - 1) + " ";

                for (int i = degree - 1; i > 1; i--)
                {
                    if (array[i - 1] != 0)
                    {
                        if (array[i - 1] > 0)
                        {
                            result += "+ " + array[i - 1] + "*x^" + (i - 1) + " ";
                        }
                        else
                        {
                            result += array[i - 1] + "*x^" + (i - 1) + " ";
                        }
                    }
                }

                if (array[0] > 0)
                {
                    result += "+ " + array[0] + " = 0";
                }
                else
                {
                    result += array[0] + " = 0";
                }
                return result; 
        }


        /* public void InputPolynomial(string name)
         {
             int degree = Degree;
             Console.WriteLine("Введите степень первого полинома " + name + ": ");
             degree = int.Parse(Console.ReadLine());
             array = new double[degree + 1];

             for (int i = 0; i <= degree; i++) //int i = degree; i >= 0; i--)
             {
                 Console.Write("Введите коэффициент при степени " + i + "(дробное):");
                 array[i] = double.Parse(Console.ReadLine());
             }
         }*/


        /// <summary>
        /// Метод, что вычисляет сумму двух полиномов.
        /// </summary>
        /// <param name="a">Передаваемый полином а</param>
        /// <param name="b">Передаваемый полином b</param>
        /// <return>Результат суммы двух полиномов - новый полином с</return>
        public static Polynom operator +(Polynom a, Polynom b)
        {
            Polynom max = Max(a, b);
            Polynom min = Min(a, b);

            //double[] c = new double[max.Degree];
            Polynom c = new Polynom(max.degree);

            for (int i = 0; i < min.degree; i++)
            {
                c[i] = max[i] + min[i];
            }

            for (int i = min.degree; i < max.degree; i++)
            {
                c[i] = max[i];
            }

            int end = max.degree;
            return c;
        }

        /// <summary>
        /// Метод, что определяет полином с max степенью.
        /// </summary>
        ///<param name="a">Передаваемый полином а</param>
        /// <param name="b">Передаваемый полином b</param>
        /// <return>Полином</return>
        private static Polynom Max(Polynom a, Polynom b)
        {
            if (a.degree > b.degree || a.degree == b.degree)
            {
                return a;
            }
            else
            {
                return b;
            }
        }

        /// <summary>
        /// Метод, что определяет полином с min степенью.
        /// </summary>
        /// <param name="a">Передаваемый полином а</param>
        /// <param name="b">Передаваемый полином b</param>
        /// <return>Полином</return>
        private static Polynom Min(Polynom a, Polynom b)
        {
            if (a.degree > b.degree || a.degree == b.degree)
            {
                return b;
            }
            else
            {
                return a;
            }
        }

        /// <summary>
        /// Метод умножения полинома а на число.
        /// </summary>
        /// <param name="a">Передаваемый полином а</param>
        /// <param name="number">Число, на которое умножается полином а</param>
        /// <return>Результат умножения полинома а на число - новый полином c</return>
        public static Polynom operator *(Polynom a, double number)
        {
            Polynom res = new Polynom(a.array.Length);
            for (int i = 0; i < a.array.Length; i++)
            {
                res.array[i] = a.array[i] * number;
            }
            return res;
        }
    }
}




/* double[] array;
 int degree;

 //свойство получения степени
 public int Degree
 {
     get { return array.Length; }
 }

 //индексатор для доступа к элементам массива
 public double this[int i]
 {
     get { return array[i]; }
     set { array[i] = value; }
 }

 //конструктор матрицы с указанием её размеров
 public Polynom(int degree)
 {
     this.degree = degree;
     array = new double[degree];
 }

 public void InputPolynomial(string name)
 {
     int degree = Degree;
     Console.WriteLine("Введите степень первого полинома " + name + ": ");
     degree = int.Parse(Console.ReadLine());
     array = new double[degree + 1];

     for (int i = 0; i <= degree; i++) //int i = degree; i >= 0; i--)
     {
         Console.Write("Введите коэффициент при степени " + i + "(дробное):");
         array[i] = double.Parse(Console.ReadLine());
     }
 }

 //правильно формируем строку полинома
 public string SPolynom
 {
     get
     {
         /*string result = "";
         if (degree == 0)
         {
             return result;
         }
         if (degree == 1)
         {
             return result += array[degree - 1] + " = 0";
         }
         result += array[degree - 1] + "*x^" + (Degree - 1) + " ";

         for (int i = degree - 1; i > 1; i--)
         {
             if (array[i - 1] != 0)
             {
                 if (array[i - 1] > 0)
                 {
                     result += "+ " + array[i - 1] + "*x^" + (i - 1) + " ";
                 }
                 else
                 {
                     result += array[i - 1] + "*x^" + (i - 1) + " ";
                 }
             }
         }

         if (array[0] > 0)
         {
             result += "+ " + array[0] + " = 0";
         }
         else
         {
             result += array[0] + " = 0";
         };
         return result;


         string result = "";
         if (Degree == 0)
         {
             return result;
         }
         if (Degree == 1)
         {
             return result += array[Degree - 1] + " = 0";
         }
         result += array[Degree - 1] + "*x^" + (Degree - 1) + " ";

         for (int i = Degree - 1; i > 1; i--)
         {
             if (array[i - 1] != 0)
             {
                 if (array[i - 1] > 0)
                 {
                     result += "+ " + array[i - 1] + "*x^" + (i - 1) + " ";
                 }
                 else
                 {
                     result += array[i - 1] + "*x^" + (i - 1) + " ";
                 }
             }
         }

         if (array[0] > 0)
         {
             result += "+ " + array[0] + " = 0";
         }
         else
         {
             result += array[0] + " = 0";
         };
         return result;

     }
 }

 public static Polynom operator +(Polynom A, Polynom B)
 {
     Polynom max = Max(A, B);
     Polynom min = Min(A, B);

     //double[] c = new double[max.Degree];
     Polynom c = new Polynom(max.Degree);

     for (int i = 0; i < min.Degree; i++)
     {
         c[i] = max[i] + min[i];
     }

     for (int i = min.Degree; i < max.Degree; i++)
     {
         c[i] = max[i];
     }

     int end = max.Degree;
     return c;
 }

 public static Polynom operator *(Polynom A, double number)
 {
     Polynom res = new Polynom(A.array.Length);
     for (int i = 0; i < A.array.Length; i++)
     {
         res.array[i] = A.array[i] * number;
     }
     return res;
 }

 private static Polynom Max(Polynom A, Polynom B)
 {
     if (A.Degree > B.Degree || A.Degree == B.Degree)
     {
         return A;
     }
     else
     {
         return B;
     }
 }

 private static Polynom Min(Polynom A, Polynom B)
 {
     if (A.Degree > B.Degree || A.Degree == B.Degree)
     {
         return B;
     }
     else
     {
         return A;
     }
 }*/



